<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th>Name</th>
        <th>Slug</th>
        <th></th>
    </tr>
    </thead>
    <tbody>

    @foreach ($categories as $category)
        <tr>
            <td>
                @for ($i = 0; $i < $category->depth; $i++) &mdash; @endfor
                <a href="{{ route('admin.category-advert.show', $category) }}">{{ $category->name }}</a>
            </td>
            <td>{{ $category->slug }}</td>
            <td>
                <div class="d-flex flex-row">
                    <form method="POST" action="{{ route('admin.category-advert.first', $category) }}" class="mr-1">
                        @csrf
                        <button class="btn btn-sm btn-outline-primary"><span class="fa fa-angle-double-up">First</span></button>
                    </form>
                    <form method="POST" action="{{ route('admin.category-advert.up', $category) }}" class="mr-1">
                        @csrf
                        <button class="btn btn-sm btn-outline-primary"><span class="fa fa-angle-up">UP</span></button>
                    </form>
                    <form method="POST" action="{{ route('admin.category-advert.down', $category) }}" class="mr-1">
                        @csrf
                        <button class="btn btn-sm btn-outline-primary"><span class="fa fa-angle-down">Down</span></button>
                    </form>
                    <form method="POST" action="{{ route('admin.category-advert.last', $category) }}" class="mr-1">
                        @csrf
                        <button class="btn btn-sm btn-outline-primary"><span class="fa fa-angle-double-down">Last</span></button>
                    </form>
                </div>
            </td>
        </tr>
    @endforeach

    </tbody>
</table>