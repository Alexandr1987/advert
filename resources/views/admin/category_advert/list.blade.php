@extends('layouts.app')

@section('content')

    <div class="container">
        <p>
            <a href="{{ route('admin.category-advert.create') }}" class="btn btn-success">Add Category Advert</a>
        </p>

        @include('admin.category_advert._category_list', ['categories'=>$categories])
    </div>
@endsection