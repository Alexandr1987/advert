@extends('layouts.app')

@section('content')
    <div class="d-flex flex-row mb-3">

        <a href="{{ route('admin.category-advert.edit', $category) }}" class="btn btn-primary mr-1">Edit</a>

        <a href="{{ route('admin.category-advert.create', ['parent' => $category]) }}" class="btn btn-success mr-1">Create</a>

        <form method="POST" action="{{ route('admin.category-advert.destroy', $category) }}" class="mr-1">
            @csrf
            @method('DELETE')
            <button class="btn btn-danger">Delete</button>
        </form>
    </div>

    <table class="table table-bordered table-striped">
        <tbody>
        <tr>
            <th>ID</th>
            <td>{{ $category->id }}</td>
        </tr>
        <tr>
            <th>Name</th>
            <td>{{ $category->name }}</td>
        </tr>
        <tr>
            <th>Slug</th>
            <td>{{ $category->slug }}</td>
        </tr>
        <tr>
            <th>Parent</th>
            <td>
                <span class="badge badge-primary">{{$category->parent ? $category->parent->name : ''}}</span>
            </td>
        </tr>
        </tbody>
    </table>

    <h3>Children category</h3>

    @include('admin.category_advert._category_list', ['categories'=>$category->descendants])

    <p><a href="{{ route('admin.category-advert.attributes.create', $category) }}" class="btn btn-success">Add Attribute</a></p>

    <table class="table table-bordered">
        <thead>
            <tr style="text-align: center">
                <th>Name</th>
                <th>Type</th>
                <th>Required</th>
                <th>Sort</th>
            </tr>
        </thead>
        <tbody>

            <tr>
                <th colspan="4" style="text-align: center">Parent attributes</th>
            </tr>

            @foreach($parentAttributes as $attribute)
                <tr>
                    <td>{{ $attribute->name }}</td>
                    <td>{{ $attribute->type }}</td>
                    <td>{{ $attribute->required ? 'Yes' : '' }}</td>
                    <td>{{ $attribute->sort }}</td>
                </tr>
            @endforeach

            <tr>
                <th colspan="4" style="text-align: center">Own attributes</th>
            </tr>

            @foreach($attributes as $attribute)
                <tr>
                    <td>
                        <a href="{{ route('admin.category-advert.attributes.show', [$category, $attribute]) }}">
                            {{ $attribute->name }}
                        </a>
                    </td>
                    <td>
                        {{ $attribute->type }}
                    </td>
                    <td>{{ $attribute->required ? 'Yes' : '' }}</td>
                    <td>{{ $attribute->sort }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection