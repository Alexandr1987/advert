@extends('layouts.app')

@section('content')

    <form method="POST" action="{{ route('admin.tickets.update', $ticket) }}">
        @csrf
        @method('PUT')

        <div class="form-group">
            <label for="subject" class="col-form-label">Subject</label>
            <input id="subject" class="form-control{{ $errors->has('subject') ? ' is-invalid' : '' }}" name="subject" value="{{ old('subject', $ticket->subject) }}" required>
            @if ($errors->has('subject'))
                <span class="invalid-feedback"><strong>{{ $errors->first('subject') }}</strong></span>
            @endif
        </div>

        <div class="form-group">
            <label for="message" class="col-form-label">Message</label>
            <textarea id="message" class="form-control{{ $errors->has('message') ? ' is-invalid' : '' }}" name="message" rows="10" required>{{ old('message', $ticket->message) }}</textarea>
            @if ($errors->has('message'))
                <span class="invalid-feedback"><strong>{{ $errors->first('message') }}</strong></span>
            @endif
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </form>

@endsection