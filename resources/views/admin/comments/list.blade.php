@extends('layouts.app')

@section('content')

    <div class="container">
        @if(count($comments)>0)
            <table class="table-bordered table-striped table">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Comment</th>
                    <th>Advert title</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($comments as $comment)
                    <tr>
                        <td>
                            {{ $comment->id }}
                        </td>
                        <td>
                            <a href="{{ route('admin.comments.view', $comment) }}">
                                {{ str_limit($comment->message, $limit = 100, $end = '...') }}
                            </a>
                        </td>
                        <td>
                            <a href="{{ route('adverts.show', $comment->advert) }}"> {{ $comment->advert->title }} </a>
                        </td>
                        <td>
                            @if($comment->isDraft())
                                <span class="badge badge-secondary">{{$comment->getStatusLabel()}}</span>
                            @elseif($comment->isAccepted())
                                <span class="badge badge-primary">{{$comment->getStatusLabel()}}</span>
                            @endif
                            @if($comment->isBlocked())
                                <span class="badge badge-danger">{{$comment->getStatusLabel()}}</span>
                            @endif
                        </td>
                        <td>

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            {{ $comments->links() }}
        @endif
    </div>
@endsection