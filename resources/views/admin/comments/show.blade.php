@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-9">

                <div class="row">
                    <label>Status: </label>
                    <strong>{{$comment->getStatusLabel()}}</strong>
                </div>

                <div class="row">
                    <label>
                        <strong>Text comment: </strong>
                    </label>


                    <p>{!! nl2br(e($comment->message)) !!}</p>
                </div>

                <div class="row">
                    <form method="POST" action="{{ route('admin.comments.delete', $comment) }}" class="mr-1">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-secondary">
                            <span class="fa fa-remove"></span> Delete
                        </button>
                    </form>

                    <form method="POST" action="{{ route('admin.comments.accepted', $comment) }}" class="mr-1">
                        @csrf
                        <button class="btn btn-danger">
                            <span class="fa fa-hand-o-up"></span> Accepted
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection