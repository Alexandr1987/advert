@extends('layouts.app')

@section('content')

    <div class="container">
        <p><a href="{{ route('admin.users.create') }}" class="btn btn-success">Add User</a></p>

        @include('admin.users.form_filter_for_list')

        <table class="table-bordered table-striped table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Email</th>
                    <th>Name</th>
                    <th>Status</th>
                    <th>Role</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->email }}</td>
                        <td><a href="{{ route('admin.users.show', $user) }}"> {{ $user->name }} </a> </td>
                        <td>
                            @if($user->isWait())
                                <span class="badge badge-secondary">Waiting</span>
                            @elseif($user->isConfirm())
                                <span class="badge badge-primary">Confirm</span>
                            @endif
                            @if($user->isBlock())
                                <span class="badge badge-danger">Block</span>
                            @endif
                        </td>
                        <td>
                            @if($user->isRoleAdmin())
                                <span class="badge badge-danger">{{ \App\Entity\User::getLabelForRole (\App\Entity\User::ROLE_ADMIN) }}</span>
                            @elseif($user->isRoleModerator())
                                <span class="badge badge-primary">{{ \App\Entity\User::getLabelForRole (\App\Entity\User::ROLE_MODERATOR) }}</span>
                            @endif
                            @if($user->isRoleUser())
                                <span class="badge badge-info">{{ \App\Entity\User::getLabelForRole (\App\Entity\User::ROLE_USER) }}</span>
                            @endif
                        </td>
                        <td>
                            <a href="{{ route('admin.users.edit', $user) }}" class="btn-sm btn-success">Edit</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        {{ $users->links() }}
    </div>
@endsection