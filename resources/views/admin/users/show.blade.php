@extends('layouts.app')

@section('content')
    <div class="d-flex flex-row mb-3">
        <a href="{{ route('admin.users.edit', $user) }}" class="btn btn-primary mr-1">Edit</a>

        @if ($user->isWait())
            <form method="POST" action="{{ route('admin.users.verify', $user) }}" class="mr-1">
                @csrf
                <button class="btn btn-success">Verify</button>
            </form>
        @endif

        @if ($user->isConfirm())
            <form method="POST" action="{{ route('admin.users.block', $user) }}" class="mr-1">
                @csrf
                <button class="btn btn-danger">Block</button>
            </form>
        @endif

        <form method="POST" action="{{ route('admin.users.destroy', $user) }}" class="mr-1">
            @csrf
            @method('DELETE')
            <button class="btn btn-danger">Delete</button>
        </form>
    </div>

    <table class="table table-bordered table-striped">
        <tbody>
        <tr>
            <th>ID</th><td>{{ $user->id }}</td>
        </tr>
        <tr>
            <th>Name</th><td>{{ $user->name }}</td>
        </tr>
        <tr>
            <th>Email</th><td>{{ $user->email }}</td>
        </tr>
        <tr>
            <th>Status</th>
            <td>
                @if ($user->isWait())
                    <span class="badge badge-secondary">Waiting</span>
                @endif
                @if ($user->isConfirm())
                    <span class="badge badge-primary">Confirm</span>
                @endif
                @if ($user->isBlock())
                    <span class="badge badge-danger">Block</span>
                @endif
            </td>
        </tr>
        <tr>
            <th>Role</th>
            <td>
                @if ($user->isRoleAdmin())
                    <span class="badge badge-danger">
                        {{\App\Entity\User::getLabelForRole(\App\Entity\User::ROLE_ADMIN)}}
                    </span>
                @endif
                @if ($user->isRoleModerator())
                    <span class="badge badge-primary">
                        {{\App\Entity\User::getLabelForRole(\App\Entity\User::ROLE_MODERATOR)}}
                    </span>
                @endif
                @if ($user->isRoleUser())
                    <span class="badge badge-info">
                        {{\App\Entity\User::getLabelForRole(\App\Entity\User::ROLE_USER)}}
                    </span>
                @endif
            </td>
        </tr>
        <tbody>
        </tbody>
    </table>
@endsection