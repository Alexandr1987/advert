@extends('layouts.app')

@section('content')

    <div class="container">
        <p>
            <a href="{{ route('admin.regions.create') }}" class="btn btn-success">Add Region</a>
        </p>

        @include('admin.regions._regions_list', ['regions'=>$regions])
    </div>
@endsection