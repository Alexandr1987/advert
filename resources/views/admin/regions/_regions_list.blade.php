@if(sizeof($regions)>0)
    <h3>Children regions(cities)</h3>
    <table class="table-bordered table-striped table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Slug</th>
                <th>Parent</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        @foreach($regions as $region)
            <tr>
                <td>{{ $region->id }}</td>
                <td>{{ $region->name }}</td>
                <td><a href="{{ route('admin.regions.show', $region) }}"> {{ $region->slug }} </a> </td>
                <td>
                    <span class="badge badge-info">{{$region->parent ? $region->parent->name : ''}}</span>
                </td>
                <td>
                    <a href="{{ route('admin.regions.edit', $region) }}" class="btn-sm btn-success">Edit</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {{ $regions->links() }}
@endif