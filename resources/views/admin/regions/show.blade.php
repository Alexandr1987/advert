@extends('layouts.app')

@section('content')
    <div class="d-flex flex-row mb-3">

        <a href="{{ route('admin.regions.edit', $region) }}" class="btn btn-primary mr-1">Edit</a>

        <a href="{{ route('admin.regions.create', ['parent' => $region]) }}" class="btn btn-success mr-1">Create</a>

        <form method="POST" action="{{ route('admin.regions.destroy', $region) }}" class="mr-1">
            @csrf
            @method('DELETE')
            <button class="btn btn-danger">Delete</button>
        </form>
    </div>

    <table class="table table-bordered table-striped">
        <tbody>
            <tr>
                <th>ID</th>
                <td>{{ $region->id }}</td>
            </tr>
            <tr>
                <th>Name</th>
                <td>{{ $region->name }}</td>
            </tr>
            <tr>
                <th>Slug</th>
                <td>{{ $region->slug }}</td>
            </tr>
            <tr>
                <th>Parent</th>
                <td>
                    <span class="badge badge-primary">{{$region->parent ? $region->parent->name : ''}}</span>
                </td>
            </tr>
        </tbody>
    </table>
    @include('admin.regions._regions_list', ['regions'=>$regions])
@endsection