@extends('layouts.app')

@section('content')

    <form method="POST" action="{{ route('admin.regions.update', $region) }}">
        @csrf
        @method('PUT')

        <div class="form-group">
            <label for="name" class="col-form-label">Name</label>
            <input id="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name', $region->name) }}" required>
            @if ($errors->has('name'))
                <span class="invalid-feedback"><strong>{{ $errors->first('name') }}</strong></span>
            @endif
        </div>

        <div class="form-group">
            <label for="email" class="col-form-label">E-Mail Address</label>
            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email', $region->email) }}" required>
            @if ($errors->has('email'))
                <span class="invalid-feedback"><strong>{{ $errors->first('email') }}</strong></span>
            @endif
        </div>

        <div class="form-group">

            <label for="status" class="col-form-label">Status</label>
            status = {{old('status')}}
            <select id="status" class="form-control{{ $errors->has('status') ? ' is-invalid' : '' }}" name="status">
                @foreach (\App\Entity\User::getStatusList() as $value))
                    <option value="{{ $value }}"{{ $value === old('status', $region->status) ? ' selected' : '' }}>
                        {{ \App\Entity\User::getLabelForStatus($value) }}
                    </option>
                @endforeach;
            </select>
            @if ($errors->has('status'))
                <span class="invalid-feedback"><strong>{{ $errors->first('status') }}</strong></span>
            @endif
        </div>

        <div class="form-group">
            <label for="status" class="col-form-label">Role</label>
            <select id="status" class="form-control{{ $errors->has('role') ? ' is-invalid' : '' }}" name="role">
                @foreach (\App\Entity\User::getRoleList() as $value))
                    <option value="{{ $value }}"{{ $value === old('role', $region->role) ? ' selected' : '' }}>
                        {{ \App\Entity\User::getLabelForRole($value) }}
                    </option>
                @endforeach;
            </select>
            @if ($errors->has('role'))
                <span class="invalid-feedback"><strong>{{ $errors->first('role') }}</strong></span>
            @endif
        </div>


        <div class="form-group">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </form>
@endsection