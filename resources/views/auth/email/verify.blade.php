@component('mail::message')
    # Confirm registration!

    <h2>Hello {{ $user->name }}</h2>

    Please, confirm your registration!

    @component('mail::button', ['url' => route('register.verify',['token'=>$user->verify_string])])
        Link to confirm registration
    @endcomponent

    Thanks,<br>
    {{ config('app.name') }}
@endcomponent
