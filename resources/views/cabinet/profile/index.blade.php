@extends('layouts.app')

@section('content')

    <div class="mb-3">
        <a href="{{ route('cabinet.profile.edit') }}" class="btn btn-primary">Edit profile</a>
    </div>

    <table class="table table-bordered">
        <tbody>
            <tr>
                <th>First Name</th><td>{{ $user->name }}</td>
            </tr>
            <tr>
                <th>Last Name</th><td>{{ $user->last_name }}</td>
            </tr>
            <tr>
                <th>Email</th><td>{{ $user->email }}</td>
            </tr>
            @if ($user->phone)
                <tr>
                    <th>Phone</th>
                    <td>{{ $user->phone }}</td>
                </tr>
            @endif
        </tbody>
    </table>
@endsection