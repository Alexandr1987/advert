@extends('layouts.app')

@section('content')

    <div class="d-flex flex-row mb-3">
        @if ($ticket->canBeRemoved())
            <form method="POST" action="{{ route('cabinet.tickets.destroy', $ticket) }}" class="mr-1">
                @csrf
                @method('DELETE')
                <button class="btn btn-danger">Delete</button>
            </form>
        @endif
    </div>

    <div class="row">
        <div class="col-md-7">
            <table class="table table-bordered table-striped">
                <tbody>
                <tr>
                    <th>ID</th>
                    <td>{{ $ticket->id }}</td>
                </tr>
                <tr>
                    <th>Created</th>
                    <td>{{ $ticket->created_at }}</td>
                </tr>
                <tr>
                    <th>Updated</th>
                    <td>{{ $ticket->updated_at }}</td>
                </tr>
                <tr>
                    <th>Status</th>
                    <td>
                        @if ($ticket->isOpen())
                            <span class="badge badge-danger">Open</span>
                        @elseif ($ticket->isApproved())
                            <span class="badge badge-primary">Active</span>
                        @elseif ($ticket->isClosed())
                            <span class="badge badge-secondary">Closed</span>
                        @endif
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="card mb-3">
        <div class="card-header">
            {{ $ticket->subject }}
        </div>
        <div class="card-body">
            {!! nl2br(e($ticket->content)) !!}
        </div>
    </div>

    <ticket :allow-message="{{ (!$ticket->allowsMessages()) ? '0' : '1'}}" :ticket-id="{{ $ticket->id }}" route="{!! route('cabinet.tickets.message', $ticket) !!}" :data="{{ $messages }}"></ticket>

@endsection