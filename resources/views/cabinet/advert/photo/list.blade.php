@extends('layouts.app')

@section('content')

    <div class="container">
        <p>
            <a href="{{ route('cabinet.advert.edit', $advert) }}" class="btn btn-danger">Edit advert</a>
        </p>

        @include('cabinet.advert.photo.upload')

        <table class="table-bordered table-striped table">
            <thead>
            <tr>
                <th>Photo</th>
                <th width="50px"></th>
            </tr>
            </thead>
            <tbody>
                @foreach($advert->photos as $photo)
                    <tr>
                        <td>
                            <a href="{{$photo->public_full_link}}" target="_blank">
                                <img src="{{$photo->public_full_link}}" width="250">
                            </a>
                        </td>
                        <td>
                            <form method="POST" action="{{ route('cabinet.advert.photo.delete', ['advert'=>$advert]) }}">
                                @csrf
                                @method('POST')
                                <input type="hidden" name="file" value="{{$photo->id}}">
                                <input type="submit" class="btn-sm btn-danger" value="Remove">
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection