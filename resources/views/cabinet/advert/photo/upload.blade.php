    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane container active" id="base">
            <form method="POST" enctype="multipart/form-data" action="{{ route('cabinet.advert.photo.upload', $advert) }}">
                @csrf
                @method('POST')
                <div class="card mb-3">
                    <div class="card-header">
                        Upload photo to advert {{ $advert->title }}
                    </div>
                    <div class="card-body pb-2">
                        <input type="file" name="photo" accept="image/*">
                        <input type="submit" value="Upload">
                    </div>
                </div>

            </form>
        </div>
    </div>