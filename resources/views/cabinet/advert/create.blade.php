@extends('layouts.app')

@section('content')

    <h2>Create advert</h2>

    <advert
            :redirect-after-success='@json(route('cabinet.advert.index'))'
            :category-list='@json($categoriesTree)'
            :category-attribute-route='@json(route('cabinet.attributes'))'
            :region-list='@json($regionsTree)'
            :action-type='@json(\App\Entity\Advert::JS_FORM_TYPE_CREATE)'
            :route='@json(route('cabinet.advert.store'))'>
    </advert>

@endsection