@extends('layouts.app')

@section('content')

    <h2>Update advert</h2>

    <advert
            :redirect-after-success='@json(route('cabinet.advert.index'))'
            :source-attributes='@json($attributes)'
            :category-list='@json($categoriesTree)'
            :category-attribute-route='@json(route('cabinet.attributes'))'
            :region-list='@json($regionsTree)'
            :action-type='@json(\App\Entity\Advert::JS_FORM_TYPE_UPDATE)'
            :advert='@json($advert)'
            :route='@json(route('cabinet.advert.update', $advert))'>
    </advert>

@endsection