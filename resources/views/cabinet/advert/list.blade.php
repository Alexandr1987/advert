@extends('layouts.app')

@section('content')

    <div class="container">
        <p>
            <a href="{{ route('cabinet.advert.create') }}" class="btn btn-danger">Add ADvert</a>
        </p>
        <table class="table-bordered table-striped table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Date</th>
                    <th>Title</th>
                    <th>Region</th>
                    <th>Category</th>
                    <th>Status</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            @foreach($adverts as $advert)
                <tr>
                    <td>{{ $advert->id }}</td>
                    <td>{{ $advert->created_at }}</td>
                    <td><a href="{{ route('cabinet.advert.show', $advert) }}"> {{ $advert->title }} </a> </td>
                    <td>{{ $advert->regions->name }}</td>
                    <td>{{ $advert->category->name }}</td>
                    <td>
                        @if ($advert->isDraft())
                            <span class="badge badge-secondary">Draft</span>
                        @elseif ($advert->isModeration())
                            <span class="badge badge-primary">Moderation</span>
                        @elseif ($advert->isActive())
                            <span class="badge badge-primary">Active</span>
                        @elseif ($advert->isClosed())
                            <span class="badge badge-secondary">Closed</span>
                        @endif
                    </td>
                    <td>
                        <a href="{{ route('cabinet.advert.edit', $advert) }}" class="btn-sm btn-success">Edit</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {{ $adverts->links() }}
    </div>
@endsection