@if (count($errors) > 0)
    <main class="py-3 align-content-center">
        <div class="alert alert-danger">
            <strong>There were some problems with your input:</strong><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </main>
@endif

@if (Session::get('success'))
    <main class="py-3 align-content-center">
        <div class="alert alert-success">
            @if(is_array(Session::get('success')))
                <ul>
                    @foreach(Session::get('success') as $msg)
                        <li>{!! $msg !!}</li>
                    @endforeach
                </ul>
            @else
                {!! Session::get('success') !!}
            @endif
        </div>
    </main>
@endif
@if (Session::get('warning'))
    <main class="py-3 align-content-center">
        <div class="alert alert-warning">
            @if(is_array(Session::get('warning')))
                <ul>
                    @foreach(Session::get('warning') as $msg)
                        <li>{!! $msg !!}</li>
                    @endforeach
                </ul>
            @else
                {!! Session::get('warning') !!}
            @endif
        </div>
    </main>
@endif
@if (Session::get('info'))
    <main class="py-3 align-content-center">
        <div class="alert alert-info">
            @if(is_array(Session::get('info')))
                <ul>
                    @foreach(Session::get('info') as $msg)
                        <li>{!! $msg !!}</li>
                    @endforeach
                </ul>
            @else
                {!! Session::get('info') !!}
            @endif
        </div>
    </main>
@endif
@if (Session::get('danger'))
    <main class="py-3 align-content-center">
        <div class="alert alert-danger">
            @if(is_array(Session::get('danger')))
                <ul>
                    @foreach(Session::get('danger') as $msg)
                        <li>{!! $msg !!}</li>
                    @endforeach
                </ul>
            @else
                {!! Session::get('danger') !!}
            @endif
        </div>
    </main>
@endif
@if (Session::get('error'))
    <main class="py-3 align-content-center">
        <div class="alert alert-danger">
            @if(is_array(Session::get('error')))
                <ul>
                    @foreach(Session::get('error') as $msg)
                        <li>{!! $msg !!}</li>
                    @endforeach
                </ul>
            @else
                {!! Session::get('error') !!}
            @endif
        </div>
    </main>
@endif
@if (Session::get('message'))
    <main class="py-3 align-content-center">
        <div class="alert alert-info">
            @if(is_array(Session::get('message')))
                <ul>
                    @foreach(Session::get('message') as $msg)
                        <li>{!! $msg !!}</li>
                    @endforeach
                </ul>
            @else
                {!! Session::get('message') !!}
            @endif
        </div>
    </main>
@endif