<nav class="navbar navbar-expand-md navbar-light navbar-laravel">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            {{ config('app.name', 'Laravel') }}
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            @can('admin-panel')
                <ul class="nav-pills nav justify-content-center">
                    <li class="nav-item ">
                        <a class="nav-link " href="{{ route('admin.home') }}">Dashboard</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="{{ route('admin.adverts.list') }}">Adverts</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="{{ route('admin.users.index') }}">Users</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="{{ route('admin.regions.index') }}">Regions</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="{{ route('admin.category-advert.index') }}">Category</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link " href="{{ route('admin.tickets.index') }}">Tickets</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link " href="{{ route('admin.comments.list') }}">Comments</a>
                    </li>
                </ul>
            @elseauth()
                <ul class="nav-pills nav justify-content-center">
                    <li class="nav-item ">
                        <a class="nav-link " href="{{ route('cabinet.advert.index') }}">Adverts</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="{{ route('cabinet.favorites.list') }}">Favorites</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="{{ route('cabinet.tickets.index') }}">Tickets</a>
                    </li>
                </ul>
            @endcan

        <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    <li class="nav-item">
                        @if (Route::has('register'))
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        @endif
                    </li>
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                            @if(Auth::user()->isRoleAdmin())
                                <a class="dropdown-item" href="{{ route('admin.home') }}">Admin</a>
                            @endif

                            @if(Auth::user()->isRoleUser())
                                <a class="dropdown-item" href="{{ route('cabinet.profile.index') }}">Profile</a>
                            @endif


                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>