@extends('layouts.app')
@section('content')
    <div class="container">

        @if ($advert->isDraft())
            <div class="alert alert-danger">
                It is a draft advert.
            </div>
            @if ($advert->reject_reason)
                <div class="alert alert-danger">
                    Advert is rejected: {{ $advert->reject_reason }}
                </div>
            @endif
        @endif


        @can('owner-advert', $advert)
            <!-- manage advert for owner advert -->
            <div class="d-flex flex-row mb-3">
                <a href="{{ route('cabinet.advert.edit', $advert) }}" class="btn btn-primary mr-1">Edit advert</a>
                <a href="{{ route('cabinet.advert.photo.list', $advert) }}" class="btn btn-primary mr-1">Photos of advert</a>

                @if ($advert->isDraft())
                    <form method="POST" action="{{ route('cabinet.advert.send', $advert) }}" class="mr-1">
                        @csrf
                        <button class="btn btn-success">Publish</button>
                    </form>
                @endif
                @if ($advert->isActive())
                    <form method="POST" action="{{ route('cabinet.advert.close', $advert) }}" class="mr-1">
                        @csrf
                        <button class="btn btn-success">Close</button>
                    </form>
                @endif

                <form method="POST" action="{{ route('cabinet.advert.destroy', $advert) }}" class="mr-1">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-danger">Delete</button>
                </form>
            </div>
        @endcan
        <div class="row">
            <div class="col-md-9">
                <p class="float-right" style="font-size: 36px;">{{ $advert->price }}$</p>
                <h1 style="margin-bottom: 10px">{{ $advert->title  }}</h1>
                <p>
                    @if ($advert->expires_at)
                        Date: {{ $advert->published_at }} &nbsp;
                    @endif
                    @if ($advert->expires_at)
                        Expires: {{ $advert->expires_at }}
                    @endif
                </p>

                <div style="margin-bottom: 20px">
                    <div class="row">
                        <div class="col-10">
                            <div style="height: 400px; background: #f6f6f6; border: 1px solid #ddd"></div>
                        </div>
                        <div class="col-2">
                            <div style="height: 100px; background: #f6f6f6; border: 1px solid #ddd"></div>
                            <div style="height: 100px; background: #f6f6f6; border: 1px solid #ddd"></div>
                            <div style="height: 100px; background: #f6f6f6; border: 1px solid #ddd"></div>
                            <div style="height: 100px; background: #f6f6f6; border: 1px solid #ddd"></div>
                        </div>
                    </div>
                </div>

                <p>{!! nl2br(e($advert->content)) !!}</p>


                <table class="table table-bordered">
                    <tbody>
                    @foreach ($advert->category->allAttributes() as $attribute)
                        <tr>
                            <th>{{ $attribute->name }}</th>
                            <td>{{ $advert->getValue($attribute->id) }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <p>Address: {{ $advert->address }}</p>

                <div style="margin: 20px 0; border: 1px solid #ddd">
                    <div id="map" style="width: 100%; height: 250px"></div>
                </div>

                <p style="margin-bottom: 20px">Seller: {{ $advert->author->name }}</p>


                @auth
                    <div class="d-flex flex-row mb-3">
                        <span class="btn btn-primary phone-button mr-1" data-source="{{ route('adverts.phone', $advert) }}">
                            <span class="fa fa-phone"></span>
                            <span class="number">Show Phone Number</span>
                        </span>

                        @if ($user->advertExistInFavorites($advert->id))
                            <form method="POST" action="{{ route('cabinet.favorites.remove', $advert) }}" class="mr-1">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-secondary">
                                    <span class="fa fa-star"></span> Remove from Favorites
                                </button>
                            </form>
                        @else
                            <form method="POST" action="{{ route('cabinet.favorites.add', $advert) }}" class="mr-1">
                                @csrf
                                <button class="btn btn-danger">
                                    <span class="fa fa-star"></span> Add to Favorites
                                </button>
                            </form>
                        @endif
                    </div>

                @endauth
                @auth
                    <comment-component
                            advert-id='{{$advert->id}}'
                            route='{{route("adverts.addComment", $advert)}}'>
                    </comment-component>

                @endauth

                @foreach($advert->comments as $comment)
                    @include('advert.comment.view', ['comment'=>$comment])
                @endforeach
            </div>
        </div>
    </div>
@endsection