<div class="card my-4">
    <p class="border p-3">
        <label>Author:</label> {{ $comment->user->name }}
        <span class="pull-right text-dark"> created:{!! e(\Illuminate\Support\Carbon::parse($comment->created_at)->diffForHumans()) !!}</span> <br>
        <span>{!! nl2br(e($comment->message)) !!}</span>
    </p>
</div>