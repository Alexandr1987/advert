<div class="card mb-3">
    <div class="card-header">
        {{ $created_at }}  by {{ $name }}
    </div>
    <div class="card-body">
        {{ $message }}
    </div>
</div>