<?php

use App\Entity\Advert;
use App\Entity\Region;
use App\Entity\User;
use App\Entity\CategoryAdvert;
use App\Entity\CategoryAdvertAttributes;
use App\Entity\Comment;
use App\Http\Router\AdvertsPath;
use App\Entity\Ticket;
use DaveJamesMiller\Breadcrumbs\BreadcrumbsGenerator as Crumbs;

Breadcrumbs::register('home', function (Crumbs $crumbs) {
    $crumbs->push('Home', route('home'));
});

// Error 404
Breadcrumbs::for('errors.404', function (Crumbs $crumbs) {
    $crumbs->parent('home');
    $crumbs->push('Page Not Found');
});

//==user regiostration and auth================
Breadcrumbs::register('login', function (Crumbs $crumbs) {
    $crumbs->parent('home');
    $crumbs->push('Login', route('login'));
});

Breadcrumbs::register('register', function (Crumbs $crumbs) {
    $crumbs->parent('home');
    $crumbs->push('Register', route('register'));
});
Breadcrumbs::register('password.request', function (Crumbs $crumbs) {
    $crumbs->parent('login');
    $crumbs->push('Reset Password', route('password.request'));
});
Breadcrumbs::register('password.reset', function (Crumbs $crumbs) {
    $crumbs->parent('password.request');
    $crumbs->push('Change', route('password.reset'));
});
//===========================================

// Admin
Breadcrumbs::register('admin.home', function (Crumbs $crumbs) {
    $crumbs->parent('home');
    $crumbs->push('Admin', route('admin.home'));
});

//=========Adverts===================
Breadcrumbs::register('admin.adverts.list', function (Crumbs $crumbs) {
    $crumbs->parent('admin.home');
    $crumbs->push('Adverts', route('admin.adverts.list'));
});
//====================================
// ==========Users===================
Breadcrumbs::register('admin.users.index', function (Crumbs $crumbs) {
    $crumbs->parent('admin.home');
    $crumbs->push('Users', route('admin.users.index'));
});

Breadcrumbs::register('admin.users.create', function (Crumbs $crumbs) {
    $crumbs->parent('admin.users.index');
    $crumbs->push('Create', route('admin.users.create'));
});
Breadcrumbs::register('admin.users.show', function (Crumbs $crumbs, User $user) {
    $crumbs->parent('admin.users.index');
    $crumbs->push($user->name, route('admin.users.show', $user));
});
Breadcrumbs::register('admin.users.edit', function (Crumbs $crumbs, User $user) {
    $crumbs->parent('admin.users.show', $user);
    $crumbs->push('Edit', route('admin.users.edit', $user));
});
//========================================
//========Comments in Admin===============
Breadcrumbs::register('admin.comments.list', function (Crumbs $crumbs) {
    $crumbs->parent('admin.home');
    $crumbs->push('Comments', route('admin.comments.list'));
});
Breadcrumbs::register('admin.comments.view', function (Crumbs $crumbs, Comment $comment) {
    $crumbs->parent('admin.comments.list');
    $crumbs->push('View', route('admin.comments.view', $comment));
});

//========================================
//====Regions=============================
Breadcrumbs::register('admin.regions.index', function (Crumbs $crumbs) {
    $crumbs->parent('admin.home');
    $crumbs->push('Regions', route('admin.regions.index'));
});
Breadcrumbs::register('admin.regions.create', function (Crumbs $crumbs) {
    $crumbs->parent('admin.regions.index');
    $crumbs->push('Create', route('admin.regions.create'));
});
Breadcrumbs::register('admin.regions.show', function (Crumbs $crumbs, Region $region) {
    if ($parent = $region->parent) {
        $crumbs->parent('admin.regions.show', $parent);
    } else {
        $crumbs->parent('admin.regions.index');
    }
    $crumbs->push($region->name, route('admin.regions.show', $region));
});
Breadcrumbs::register('admin.regions.edit', function (Crumbs $crumbs, Region $region) {
    $crumbs->parent('admin.category-advert.show', $region);
    $crumbs->push('Edit', route('admin.regions.edit', $region));
});
//=========================================
//=== Category Advert======================
Breadcrumbs::register('admin.category-advert.index', function (Crumbs $crumbs) {
    $crumbs->parent('admin.home');
    $crumbs->push('Category Advert', route('admin.category-advert.index'));
});
Breadcrumbs::register('admin.category-advert.create', function (Crumbs $crumbs) {
    $crumbs->parent('admin.category-advert.index');
    $crumbs->push('Create', route('admin.category-advert.create'));
});
Breadcrumbs::register('admin.category-advert.show', function (Crumbs $crumbs, CategoryAdvert $categoryAdvert) {
    if ($parent = $categoryAdvert->parent) {
        $crumbs->parent('admin.category-advert.show', $parent);
    } else {
        $crumbs->parent('admin.category-advert.index');
    }
    $crumbs->push($categoryAdvert->name, route('admin.category-advert.show', $categoryAdvert));
});
Breadcrumbs::register('admin.category-advert.edit', function (Crumbs $crumbs, CategoryAdvert $categoryAdvert) {
    $crumbs->parent('admin.category-advert.show', $categoryAdvert);
    $crumbs->push('Edit', route('admin.category-advert.edit', $categoryAdvert));
});
//======================= admin - Tickets==============================================
Breadcrumbs::register('admin.tickets.index', function (Crumbs $crumbs) {
    $crumbs->parent('admin.home');
    $crumbs->push('Tickets', route('admin.tickets.index'));
});
Breadcrumbs::register('admin.tickets.show', function (Crumbs $crumbs, Ticket $ticket) {
    $crumbs->parent('admin.tickets.index');
    $crumbs->push($ticket->subject, route('admin.tickets.show', $ticket));
});
Breadcrumbs::register('admin.tickets.edit', function (Crumbs $crumbs, Ticket $ticket) {
    $crumbs->parent('admin.tickets.show', $ticket);
    $crumbs->push('Edit', route('admin.tickets.edit', $ticket));
});
//======================================================================================

//================Category Advert Attributes============================================
Breadcrumbs::register('admin.category-advert.attributes.create', function (Crumbs $crumbs, CategoryAdvert $category) {
    $crumbs->parent('admin.category-advert.show', $category);
    $crumbs->push('Create', route('admin.category-advert.attributes.create', $category));
});
Breadcrumbs::register('admin.category-advert.attributes.show', function (Crumbs $crumbs, CategoryAdvert $category, CategoryAdvertAttributes $attribute) {
    $crumbs->parent('admin.category-advert.show', $category);
    $crumbs->push($attribute->name, route('admin.category-advert.attributes.show', [$category, $attribute]));
});
Breadcrumbs::register('admin.category-advert.attributes.edit', function (Crumbs $crumbs, CategoryAdvert $category, CategoryAdvertAttributes $attribute) {
    $crumbs->parent('admin.category-advert.attributes.show', $category, $attribute);
    $crumbs->push('Edit', route('admin.category-advert.attributes.edit', [$category, $attribute]));
});
//=====================================================================
//===User Profile======================================================
Breadcrumbs::register('cabinet.profile.index', function (Crumbs $crumbs) {
    $crumbs->parent('home');
    $crumbs->push('Profile', route('cabinet.profile.index'));
});

Breadcrumbs::register('cabinet.profile.edit', function (Crumbs $crumbs) {
    $crumbs->parent('cabinet.profile.index');
    $crumbs->push('Edit', route('cabinet.profile.edit'));
});
//=====================================================================
//===========User Advert================================================
Breadcrumbs::register('cabinet.advert.index', function (Crumbs $crumbs) {
    $crumbs->parent('home');
    $crumbs->push('Advert', route('cabinet.advert.index'));
});
Breadcrumbs::register('cabinet.advert.create', function (Crumbs $crumbs) {
    $crumbs->parent('cabinet.advert.index');
    $crumbs->push('Create', route('cabinet.advert.create'));
});

Breadcrumbs::register('cabinet.advert.show', function (Crumbs $crumbs, Advert $advert) {
    $crumbs->parent('cabinet.advert.index', $advert);
    $crumbs->push($advert->title, route('cabinet.advert.show', $advert));
});

Breadcrumbs::register('cabinet.advert.edit', function (Crumbs $crumbs, Advert $advert) {
    $crumbs->parent('cabinet.advert.show', $advert);
    $crumbs->push('Edit', route('cabinet.advert.edit', $advert));
});
//=====================================================================
// Cabinet Tickets
Breadcrumbs::register('cabinet.tickets.index', function (Crumbs $crumbs) {
    $crumbs->parent('home');
    $crumbs->push('Tickets', route('cabinet.tickets.index'));
});
Breadcrumbs::register('cabinet.tickets.create', function (Crumbs $crumbs) {
    $crumbs->parent('cabinet.tickets.index');
    $crumbs->push('Create', route('cabinet.tickets.create'));
});
Breadcrumbs::register('cabinet.tickets.show', function (Crumbs $crumbs, Ticket $ticket) {
    $crumbs->parent('cabinet.tickets.index');
    $crumbs->push($ticket->subject, route('cabinet.tickets.show', $ticket));
});
//======== advert photo========================================
Breadcrumbs::register('cabinet.advert.photo.list', function (Crumbs $crumbs, Advert $advert) {
    $crumbs->parent('cabinet.advert.edit', $advert);
    $crumbs->push('Photos', route('cabinet.advert.photo.list', $advert));
});

//=============================================================
//================ADVERT==========================================
// Adverts
Breadcrumbs::register('adverts.inner_region', function (Crumbs $crumbs, AdvertsPath $path) {
    if ($path->region && $parent = $path->region->parent) {
        $crumbs->parent('adverts.inner_region', $path->withRegion($parent));
    } else {
        $crumbs->parent('home');
        $crumbs->push('Adverts', route('adverts.index'));
    }
    if ($path->region) {
        $crumbs->push($path->region->name, route('adverts.index', $path));
    }
});
Breadcrumbs::register('adverts.inner_category', function (Crumbs $crumbs, AdvertsPath $path, AdvertsPath $orig) {
    if ($path->category && $parent = $path->category->parent) {
        $crumbs->parent('adverts.inner_category', $path->withCategory($parent), $orig);
    } else {
        $crumbs->parent('adverts.inner_region', $orig);
    }
    if ($path->category) {
        $crumbs->push($path->category->name, route('adverts.index', $path));
    }
});
Breadcrumbs::register('adverts.index', function (Crumbs $crumbs, AdvertsPath $path = null) {
    $path = $path ?: adverts_path(null, null);
    $crumbs->parent('adverts.inner_category', $path, $path);
});

Breadcrumbs::register('adverts.show', function (Crumbs $crumbs, Advert $advert) {
    $crumbs->parent('adverts.index', $advert->region, $advert->category);
    $crumbs->push($advert->title, route('adverts.show', $advert));
});
///=============FAVORITE ADVERT ======================================
Breadcrumbs::register('cabinet.favorites.list', function (Crumbs $crumbs) {
    $crumbs->parent('cabinet.advert.index');
    $crumbs->push('Favorites', route('cabinet.favorites.list'));
});
/// ==================================================================
?>