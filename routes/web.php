<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/verify/{token}', 'Auth\RegisterController@verify')->name('register.verify');

Route::group([
    'prefix' => 'adverts',
    'as' => 'adverts.',
    'namespace' => 'Advert',
    ], function () {
        Route::get('/show/{advert}', 'AdvertController@show')->name('show');
        Route::post('/show/{advert}/phone', 'AdvertController@phone')->name('phone');
        //Route::get('/{category?}', 'AdvertController@index')->name('index');
        Route::post('/show/{advert}/comment', 'AdvertController@addComment')->name('addComment');
        Route::get('/{adverts_path?}', 'AdvertController@index')->name('index')->where('adverts_path', '.+');
});

//admin area
\Illuminate\Support\Facades\Route::group(
    [
        'prefix' => 'admin',
        'as' => 'admin.',
        'middleware' => ['auth', 'can:admin-panel'],
        'namespace' => 'Admin',
    ],
    function (){
        \Illuminate\Support\Facades\Route::get('/', 'HomeController@index')->name('home');
        \Illuminate\Support\Facades\Route::resource('users', 'UsersController');
        \Illuminate\Support\Facades\Route::post('/users/{user}/verify', 'UsersController@verify')->name('users.verify');
        \Illuminate\Support\Facades\Route::post('/users/{user}/block', 'UsersController@block')->name('users.block');

        \Illuminate\Support\Facades\Route::resource('regions', 'RegionsController');

        \Illuminate\Support\Facades\Route::resource('category-advert', 'CategoryAdvertController');

        //admin control category of adverts
        Route::group(['prefix' => 'category-advert/{category}', 'as' => 'category-advert.'], function () {
            Route::post('/first', 'CategoryAdvertController@first')->name('first');
            Route::post('/up', 'CategoryAdvertController@up')->name('up');
            Route::post('/down', 'CategoryAdvertController@down')->name('down');
            Route::post('/last', 'CategoryAdvertController@last')->name('last');
            Route::resource('attributes', 'CategoryAdvertAttributesController')->except('index');
        });
        //admin control commments
        Route::group(['prefix' => 'comment', 'as' => 'comments.'], function () {
            \Illuminate\Support\Facades\Route::get('/', 'CommentsController@index')->name('list');
            \Illuminate\Support\Facades\Route::get('/show/{comment}', 'CommentsController@show')->name('view');
            \Illuminate\Support\Facades\Route::delete('/delete/{comment}', 'CommentsController@delete')->name('delete');
            \Illuminate\Support\Facades\Route::post('/accepted/{comment}', 'CommentsController@accepted')->name('accepted');
        });

        Route::group(['prefix' => 'adverts', 'as' => 'adverts.'], function () {
            \Illuminate\Support\Facades\Route::get('/', 'AdvertController@index')->name('list');
        });

        Route::group(['prefix' => 'tickets', 'as' => 'tickets.'], function () {
            Route::get('/', 'TicketController@index')->name('index');
            Route::get('/{ticket}/show', 'TicketController@show')->name('show');
            Route::get('/{ticket}/edit', 'TicketController@editForm')->name('edit');
            Route::put('/{ticket}/edit', 'TicketController@edit')->name('update');
            Route::post('{ticket}/message', 'TicketController@message')->name('message');
            Route::post('/{ticket}/close', 'TicketController@close')->name('close');
            Route::post('/{ticket}/approve', 'TicketController@approve')->name('approve');
            Route::post('/{ticket}/reopen', 'TicketController@reopen')->name('reopen');
            Route::delete('/{ticket}/destroy', 'TicketController@destroy')->name('destroy');
        });
    }
);

//user profile
//auth area
\Illuminate\Support\Facades\Route::group(
    [
        'prefix' => 'cabinet',
        'as' => 'cabinet.',
        'middleware' => ['auth'],
        'namespace' => 'Cabinet',
    ],
    function () {
        Route::get('/', 'ProfileController@index')->name('profile.index');
        Route::get('/edit', 'ProfileController@edit')->name('profile.edit');
        Route::put('/update', 'ProfileController@update')->name('profile.update');
        Route::resource('advert', 'AdvertController')->except(['photoUpload']);
        Route::post('attributes','AdvertController@attributesByCategory')->name('attributes');

        Route::group(['prefix' => 'advert', 'as' => 'advert.'], function () {
            Route::post('/{advert}/send', 'AdvertController@send')->name('send');
            Route::post('/{advert}/close', 'AdvertController@close')->name('close');
            Route::delete('/{advert}/destroy', 'AdvertController@destroy')->name('destroy');
        });


        Route::group(['prefix' => 'advert', 'as' => 'chat-message.'], function () {
            Route::post('/advert', 'ChatMessageController@advert')->name('advert');
        });


        Route::group(['prefix'=> 'advert/favorites','as'=>'favorites.'], function (){
            Route::get('/list', 'FavoritesController@index')->name('list');
            Route::post('/{advert}/add-favorites', 'FavoritesController@add')->name('add');
            Route::delete('/{advert}/remove-favorites', 'FavoritesController@remove')->name('remove');
        });

        Route::group(['prefix' => 'advert/{advert}', 'as'=>'advert.'], function (){
            Route::post('/photo-upload','AdvertPhotoController@upload')->name('photo.upload');
            Route::get('/list','AdvertPhotoController@index')->name('photo.list');
            Route::post('/delete','AdvertPhotoController@delete')->name('photo.delete');
        });

        Route::resource('tickets', 'TicketController')->only(['index', 'show', 'create', 'store', 'destroy']);
        Route::post('tickets/{ticket}/message', 'TicketController@message')->name('tickets.message');
    });
