<?php

namespace App\Console\Commands\Advert;

use App\Entity\Advert;
use App\Service\AdvertService;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ExpireCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'advert:expire';

    protected $service;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(AdvertService $advertService)
    {
        parent::__construct();

        $this->service = $advertService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $success = true;
        foreach (Advert::byActive()->where('expired_at', '<', Carbon::now())->cursor() as $advert) {
            try {
                $this->service->expire($advert);
            } catch (\DomainException $e) {
                $this->error($e->getMessage());
                $success = false;
            }
        }
        return $success;
    }
}
