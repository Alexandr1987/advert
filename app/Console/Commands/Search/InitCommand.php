<?php

namespace App\Console\Commands\Search;

use App\Service\AdvertIndexElasticSearchService;
use Elasticsearch\Client;
use Elasticsearch\Common\Exceptions\Missing404Exception;
use Illuminate\Console\Command;

class InitCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'search:init';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create index in Elastic Search';

    protected $client;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Client $client)
    {
        parent::__construct();

        $this->client = $client;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(): bool
    {
        //remove exist index in elastic
        try{
            $this->client->indices()->delete(['index' => AdvertIndexElasticSearchService::NAME_INDEX]);
        }catch (Missing404Exception $exception){

        }

        //create new index
        try{
            $this->client->indices()->create(
                [
                    'index' => AdvertIndexElasticSearchService::NAME_INDEX,
                    'body' => [
                        'mapping' => [
                            AdvertIndexElasticSearchService::NAME_TYPE=>[
                                '_source' => [
                                    'enabled' => true,
                                ],
                                'properties' => [
                                    'id' => [
                                        'type' => 'integer',
                                    ],
                                    'published_at' => [
                                        'type' => 'date',
                                    ],
                                    'title' => [
                                        'type' => 'text',
                                    ],
                                    'content' => [
                                        'type' => 'text',
                                    ],
                                    'price' => [
                                        'type' => 'integer',
                                    ],
                                    'status' => [
                                        'type' => 'keyword',
                                    ],
                                    'category' => [
                                        'type' => 'integer',
                                    ],
                                    'regions' => [
                                        'type' => 'integer',
                                    ],
                                ],
                            ],
                        ],
                        'settings' => [
                            'analysis' => [
                                'char_filter' => [
                                    'replace' => [
                                        'type' => 'mapping',
                                        'mappings' => [
                                            '&=> and '
                                        ],
                                    ],
                                ],
                                'filter' => [
                                    'word_delimiter' => [
                                        'type' => 'word_delimiter',
                                        'split_on_numerics' => false,
                                        'split_on_case_change' => true,
                                        'generate_word_parts' => true,
                                        'generate_number_parts' => true,
                                        'catenate_all' => true,
                                        'preserve_original' => true,
                                        'catenate_numbers' => true,
                                    ],
                                    'trigrams' => [
                                        'type' => 'ngram',
                                        'min_gram' => 4,
                                        'max_gram' => 6,
                                    ],
                                ],
                                'analyzer' => [
                                    'default' => [
                                        'type' => 'custom',
                                        'char_filter' => [
                                            'html_strip',
                                            'replace',
                                        ],
                                        'tokenizer' => 'whitespace',
                                        'filter' => [
                                            'lowercase',
                                            'word_delimiter',
                                            'trigrams',
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ]
                ]
            );
        }catch (Missing404Exception $exception){
            echo $exception->getMessage();
        }

        return true;
    }
}
