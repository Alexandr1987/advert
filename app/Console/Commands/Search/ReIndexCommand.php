<?php

namespace App\Console\Commands\Search;

use App\Entity\Advert;
use App\Service\AdvertIndexElasticSearchService;
use Illuminate\Console\Command;

class ReIndexCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'search:reindex';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear all info about adverts in ES and add all adverts to index';

    protected $advertIndex;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(AdvertIndexElasticSearchService $advertIndex)
    {
        parent::__construct();

        $this->advertIndex = $advertIndex;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(): bool
    {
        //remove all data from elasticsearch
        $this->advertIndex->clear();

        //get all adverts and add in index of ES
        //TODO add filter by only active=1, adverts to add in index ES
        foreach (Advert::orderBy('id')->cursor() as $advert) {
            $this->advertIndex->index($advert);
        }

        return true;
    }
}
