<?php

namespace App\Console\Commands\User;

use App\Entity\User;
use Illuminate\Console\Command;

class ChangeRoleCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:change_role {email} {role}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change role for user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(): void
    {
        $email = $this->argument('email');

        $role = $this->argument('role');

        $user = \App\Entity\User::where('email', $email)->first();

        if(empty($user)){
            $this->error('Not find user by email - '.$email);
        }

        if(!empty($user)){
            try{
                $user->changeRole($role);
                $this->info('Success change role for user - '.$email);
            }catch (\LogicException $exception){
                $this->error($exception->getMessage());
            }
        }
    }
}
