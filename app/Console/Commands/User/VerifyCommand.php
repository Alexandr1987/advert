<?php

namespace App\Console\Commands\User;

use Illuminate\Console\Command;
use App\Entity\User;

class VerifyCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:verify {email}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Verify user by email';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $email = $this->argument('email');

        $user = \App\Entity\User::where('email', $email)->first();

        if(empty($user)){
            $this->error('Not find user by email - '.$email);
        }

        if(!empty($user)){
            if($user->isWait()){

                try{
                    User::verifyUserById($user->id);
                    $this->info('Success confirm user - '.$email);
                }catch (\LogicException $exception){
                    $this->error($exception->getMessage());
                }
            }else{
                $this->error('User have status is not wait');
            }
        }
    }
}
