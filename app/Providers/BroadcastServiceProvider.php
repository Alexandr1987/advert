<?php

namespace App\Providers;

use App\Entity\Ticket;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Broadcast;
use Illuminate\Broadcasting\PrivateChannel;

class BroadcastServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //Broadcast::routes(['middleware'=>['auth', 'web']]);//['middleware'=>['auth','web']]
        Broadcast::routes();

        /*
         * Authenticate the user's personal channel...
         */
        Broadcast::channel('ticket.message.{ticketId}', function ($user, $ticketId) {
            //for admin all access
            if($user->isRoleAdmin() || $user->isRoleModerator()){
                return true;
            }else{
                //check access for auth user
                $ticket = Ticket::findOrFail($ticketId);
                //Log::info (print_r ($ticket, 1));
                if($ticket->user_id == $user->id){
                    return true;
                }
            }
        });

        //require base_path('routes/channels.php');
    }
}
