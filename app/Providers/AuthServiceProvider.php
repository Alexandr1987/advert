<?php

namespace App\Providers;

use App\Entity\Advert;
use App\Entity\Ticket;
use App\Entity\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Horizon\Horizon;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        $this->setAccessRules();

        Horizon::auth(function (){
            return Gate::allows('horizon');
        });
    }

    private function setAccessRules()
    {
        //access in admin panel
        Gate::define('admin-panel', function (User $user){
            return $user->isRoleAdmin();
        });

        //edit advert for owner
        Gate::define('owner-advert', function (User $user, Advert $advert){
            return $advert->author_id === $user->id;
        });

        //owner for ticket
        Gate::define('owner-ticket', function (User $user, Ticket $ticket){
            return $ticket->user_id === $user->id;
        });

        //access for horizon
        Gate::define('horizon', function (User $user){
            return $user->isRoleAdmin();
        });
    }
}
