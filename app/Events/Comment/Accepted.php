<?php

namespace App\Events\Comment;

use App\Entity\Advert;
use App\Entity\Comment;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class Accepted
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $comment;
    public $advert;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Comment $comment, Advert $advert)
    {
        /** @var Advert advert */
        $this->advert = $advert;
        /** @var Comment comment */
        $this->comment = $comment;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('');
    }
}
