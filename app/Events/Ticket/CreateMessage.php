<?php

namespace App\Events\Ticket;

use App\Entity\TicketMessage;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class CreateMessage implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $message;
    public $ticketId;
    public $userName;
    public $created_at;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(int $ticketId, TicketMessage $message)
    {
        $message = $message->load(['user']);

        $this->message = $message->message;

        $this->created_at = $message->created_at;

        $this->userName  = $message->user->name;

        $this->ticketId = $ticketId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('ticket.message.' . $this->ticketId);
    }

    public function broadcastAs()
    {
        return 'message.create';
    }

    public function broadcastWith()
    {
        return [
            'view' => view('ticket.onemessage', ['created_at' => $this->created_at, 'message'=> $this->message, 'name'=>$this->userName])->render (),
        ];
    }
}
