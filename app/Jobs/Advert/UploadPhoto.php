<?php

namespace App\Jobs\Advert;

use App\Entity\PhotoAdvert;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Storage;
use Spatie\Dropbox\Client;
use GuzzleHttp\Client  as GuzzleClient;
use GuzzleHttp\RequestOptions;

class UploadPhoto implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $path;
    protected $photoId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $path, int $photoAdvertId)
    {
        $this->photoId = $photoAdvertId;
        $this->path = $path;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->photo = PhotoAdvert::findOrFail($this->photoId);

        $dropboxClient = new Client(env('DROPBOX_AUTHORIZATION_TOKEN'));

        $contentFile = Storage::disk('local')->get($this->path);

        $upload = $dropboxClient->upload($this->path, $contentFile);

        unset($contentFile);

        $sharedLinks = $dropboxClient->listSharedLinks($upload['path_lower'], true);

        $publicFullPath = '';

        if(!empty($sharedLinks[0])){
            if(!empty($sharedLinks[0]['url'])){
                $publicFullPath = $sharedLinks[0]['url'];
            }
        }

        if(empty($publicFullPath)){
            $publicFullPath = $dropboxClient->createSharedLinkWithSettings($this->path, ['requested_visibility' => 'public']);
        }

        //redirect on link in dropbox and get full shared link
        $publicFullPath = $this->getCompleteShareLink($publicFullPath);

        $this->photo->update(['status'=>PhotoAdvert::STATUS_COMPLETE_UPLOAD, 'public_full_link'=>$publicFullPath]);

        if(!empty($publicFullPath)){
            //remove file from local storage - if upload on clound
            $exist = Storage::disk('local')->exists($this->path);
            if($exist){
                Storage::disk('local')->delete($this->path);
            }
        }
    }

    protected function getCompleteShareLink(string $shareLink): string
    {
        $clinetHttp = new GuzzleClient([RequestOptions::ALLOW_REDIRECTS => true]);

        //change url for shared link - for redirect on full link
        $response = $clinetHttp->get($shareLink . '0&raw=1');

        $completeShareLinkOnImage = '';

        $res = $clinetHttp->get($shareLink . '0&raw=1',[
            'on_stats'=>function (\GuzzleHttp\TransferStats $stats) use(&$completeShareLinkOnImage){
                $completeShareLinkOnImage = (string) $stats->getEffectiveUri();
            }
        ]);

        return $completeShareLinkOnImage;
    }

    public function tags()
    {
        return ['uploadPhoto', 'dropbox:'.$this->photoId];
    }
}
