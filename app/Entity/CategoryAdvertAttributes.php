<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property integer $advert_category_id
 * @property string $type
 * @property bool $required
 * @property array $variants
 * @property integer $sort
 */

class CategoryAdvertAttributes extends Model
{
    const TYPE_INTEGER = 'integer';
    const TYPE_FLOAT = 'float';
    const TYPE_STRING = 'string';
    const TYPE_DROPDOWN = 'dropdown';

    protected $table = 'advert_category_attributes';

    public $timestamps = false;

    protected $appends = ['value'];
    public $value;

    protected $fillable = ['advert_category_id', 'name', 'type', 'required', 'variants', 'sort'];

    //convert value from json
    protected $casts = [
        'variants' => 'array',
    ];

    public function getValueAttribute()
    {
        return $this->value;
    }

    public function setValueAttribute($value)
    {
        $this->value = $value;
    }

    static function getTypeList()
    {
        return [
            self::TYPE_STRING => 'String',
            self::TYPE_INTEGER => 'Integer',
            self::TYPE_FLOAT => 'Float',
            self::TYPE_DROPDOWN => 'Dropdown',
        ];
    }

    public function isFloat():bool
    {
        return $this->type === self::TYPE_FLOAT;
    }

    public function isInteger():bool
    {
        return $this->type === self::TYPE_INTEGER;
    }

    public function isString():bool
    {
        return $this->type === self::TYPE_STRING;
    }

    public function isSelectList(): bool
    {
        if(count($this->variants)>1){
            return true;
        }
        return false;
    }

    public function getLabelOfType(): string
    {
        if(in_array($this->type, self::getTypeList())){
            if($this->isFloat()){
                return self::TYPE_FLOAT;
            }
            if($this->isInteger()){
                return self::TYPE_INTEGER;
            }
            if($this->isString()){
                return self::TYPE_STRING;
            }
        }
        return 'Not find label for type';
    }
}
