<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;
use App\Entity\CategoryAdvertAttributes as Attributes;

/**
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property integer $parent_id
 *
 * @property int $depth
 * @property CategoryAdvert $parent
 * @property CategoryAdvert[] $children
 * @property CategoryAdvertAttributes[] $attributes
 */

class CategoryAdvert extends Model
{
    use NodeTrait;

    protected $table = 'advert_category';

    public $timestamps = false;

    protected $fillable = ['name', 'slug', 'parent_id'];

    public function parentAttributes(): array
    {
        return $this->parent ? $this->parent->allAttributes() : [];
    }

    /**
     * @return CategoryAdvertAttributes[]
     */
    public function allAttributes(): array
    {
        return array_merge($this->parentAttributes(), $this->attributes()->orderBy('sort')->getModels());
    }

    public function allAttributesWithValues(array $attributes, Advert $advert)
    {
        foreach ($attributes as $attribute){
            $attribute->value = $advert->getValue($attribute->id);
        }
        return $attributes;
    }

    public function attributes()
    {
        return $this->hasMany(Attributes::class, 'advert_category_id', 'id')
            ->orderBy('sort')
            ->select(['id','name','type','required','variants']);
    }

    //get regions list for dropdown in form
    static function getCategoryList(): array
    {
        return CategoryAdvert::all(['id','name'])->sortBy('name')->toArray();
    }

    static function getCategoryTree($idSelectedCategory = null): array
    {
        //get all levels of tree category
        $tree = self::defaultOrder()->get()->toTree();

        $data = [];

        //formate array with labels
        foreach ($tree as $node)
        {
            if($node->children->count() > 0){
                $row = [
                    'text' => $node->name,
                    'children'=> self::subTree ($node->children, $idSelectedCategory),
                    'id' => $node->id,
                ];

                if($idSelectedCategory == $node->id){
                    $row['state'] = ['selected'=>true];
                }

                $data[] = $row;
            }else{

                $row = ['text' => $node->name,'id' => $node->id];

                if($idSelectedCategory == $node->id){
                    $row['state'] = ['selected'=>true];
                }

                $data[] = $row;
            }
        }
        return $data;
    }

    public function getPath(): string
    {
        return implode('/', array_merge($this->ancestors()->defaultOrder()->pluck('slug')->toArray(), [$this->slug]));
    }

    static function subTree($nodes, $idSelectedCategory = null): array
    {
        $data = [];

        foreach ($nodes as $node){
            if($node->children->count() > 0){
                $row = [];
                if($idSelectedCategory == $node->id){
                    $row['state'] = ['selected'=>true];
                }

                $data[] = [
                    'text' => $node->name,
                    'children' => self::subTree ($node->children, $idSelectedCategory),
                    'id' => $node->id
                ]+$row;
            }else{
                $row = [];
                if($idSelectedCategory == $node->id){
                    $row['state'] = ['selected'=>true];
                }

                $data[] = ['text' => $node->name,'id' => $node->id]+$row;
            }
        }

        return $data;
    }
}
