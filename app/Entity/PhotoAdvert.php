<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class PhotoAdvert extends Model
{
    const STATUS_CREATED = 0;
    const STATUS_START_UPLOAD = 1;
    const STATUS_COMPLETE_UPLOAD = 2;
    const STATUS_START_DELETE = 3;
    const STATUS_COMPLETE_DELETE = 4;

    protected $table = 'advert_photo';

    protected $fillable = ['advert_id', 'file', 'public_full_link', 'storage', 'status'];

    public $timestamps = false;

    public function isCompleteUpload(): bool
    {
        return $this->status === self::STATUS_COMPLETE_UPLOAD;
    }

    public function isStartUpload(): bool
    {
        return $this->status === self::STATUS_START_UPLOAD;
    }

    public function isStartDelete(): bool
    {
        return $this->status === self::STATUS_START_DELETE;
    }

    public function isCompleteDelete(): bool
    {
        return $this->status === self::STATUS_COMPLETE_DELETE;
    }

    public function isOnProcess(): bool
    {
        return $this->isStartUpload() || $this->isStartDelete();
    }
}
