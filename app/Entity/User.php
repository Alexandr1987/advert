<?php

namespace App\Entity;

use App\Notifications\VerifyEmail;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Auth\Notifications;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

/**
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $status
 * @property string $role
 * @property string $last_name
 * @property string $phone
 * @property string $password
 * @property string $remember_token
 * @property string $verify_string;
 */

class User extends Authenticatable
{
    use Notifiable;

    const STATUS_WAIT = 'wait';
    const STATUS_CONFIRM = 'confirm';
    const STATUS_BLOCK = 'block';

    const LABEL_FOR_STATUS = [
        self::STATUS_BLOCK => 'Block',
        self::STATUS_CONFIRM => 'Confirm',
        self::STATUS_WAIT => 'Not confirm',
    ];

    const ROLE_ADMIN = 'admin';
    const ROLE_USER = 'user';
    const ROLE_MODERATOR = 'moderator';

    const LABEL_FOR_ROLE = [
        self::ROLE_USER => 'User',
        self::ROLE_MODERATOR => 'Moderator',
        self::ROLE_ADMIN => 'Administrator',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'status' , 'verify_string', 'role', 'phone', 'last_name'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'verify_string', 'status',
    ];

    static function register(string $name, string $email, string $password): self
    {
        $user = static::create ( [
            'name' => $name,
            'email' => $email,
            'password' => Hash::make ( $password ),
            'status' => User::STATUS_WAIT,
            'verify_string' => Str::random ( 32 ),
            'role' => self::ROLE_USER,
        ] );
        return $user;
    }

    static function getStatusList(): array
    {
        return [
            self::STATUS_WAIT,
            self::STATUS_BLOCK,
            self::STATUS_CONFIRM,
        ];
    }

    static function getRoleList(): array
    {
        return [
            self::ROLE_USER,
            self::ROLE_MODERATOR,
            self::ROLE_ADMIN,
        ];
    }

    static function getLabelForStatus(string $status): string
    {
        $list = self::getStatusList();

        if(in_array($status, $list)){
            return self::LABEL_FOR_STATUS[$status];
        }
        return 'Not find label';
    }

    static function getLabelForRole(string $role)
    {
        $listRoles = self::getRoleList ();
        if(in_array($role, $listRoles)){
            return self::LABEL_FOR_ROLE[$role];
        }
        return 'Not find label';
    }

    public function isRoleAdmin(): bool
    {
        return $this->role === self::ROLE_ADMIN;
    }

    public function isRoleModerator(): bool
    {
        return $this->role === self::ROLE_MODERATOR;
    }

    public function isRoleUser(): bool
    {
        return $this->role === self::ROLE_USER;
    }

    public function hasVerifiedEmail()
    {

    }

    public function isFilledProfile(): bool
    {
        //if user complete fill all profile field - return true
        if(!empty($this->name) && !empty($this->phone) && !empty($this->last_name) && $this->isConfirm()){
            return true;
        }
        return false;
    }

    public function isBlock(): bool
    {
        return $this->status === self::STATUS_BLOCK;
    }

    public function isConfirm(): bool
    {
        return $this->status === self::STATUS_CONFIRM;
    }

    public function isWait(): bool
    {
        return $this->status === self::STATUS_WAIT;
    }

    public function changeStatus($status): void
    {
        if(in_array($status, self::getStatusList())){
            $this->status = $status;
            $this->save();
        }
    }

    public function changeRole($role): void
    {
        if(in_array($role, self::getRoleList())){
            if($this->role === $role){
                throw new \LogicException('This role exist in user');
            }
            $this->role = $role;
            $this->save();
        }else{
            throw  new \LogicException('Role is '.$role . ' not exist in role list');
        }
    }

    public static function verifyUserById($id): void
    {
        //find user by id
        $user = User::where('id', $id)->first();

        if(is_null($user->verify_string)){
            throw new \LogicException('Not find user by token');
        }

        //user is not status-wait
        if(!$user->isWait()){
            throw new \LogicException('Your email is already verified.');
        }

        //update status user - verified user
        $user->status = self::STATUS_CONFIRM;
        $user->verify_string = null;
        $user->save();
    }

    public function isPhoneVerified():bool
    {
        return false;
    }

    public function favorites()
    {
        return $this->belongsToMany(Advert::class, 'adverts_favorite', 'user_id', 'advert_id');
    }

    public function advertExistInFavorites(int $advertId): bool
    {
        return $this->favorites()->where('advert_id', $advertId)->exists();
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
}
