<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class AdvertValues extends Model
{
    protected $table = 'advert_values';

    public $timestamps = false;

    protected $fillable = ['attribute_id', 'value'];
}
