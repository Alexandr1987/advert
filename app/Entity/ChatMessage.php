<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class ChatMessage extends Model
{
    protected $table = 'advert_chat_message';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];
}
