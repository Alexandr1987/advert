<?php

namespace App\Entity;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property integer $parent_id
 *
 * @property Region $parent
 * @property Region $children
 */

class Region extends Model
{
    use Sluggable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'slug', 'parent_id',
    ];

    public function getPath(): string
    {
        return ($this->parent ? $this->parent->getPath() . '/' : '') . $this->slug;
    }

    public function parent()
    {
        return $this->belongsTo(static::class, 'parent_id', 'id');
    }

    public function children()
    {
        return $this->hasMany(static::class, 'parent_id', 'id');
    }

    //get regions list for dropdown in form
    static function getRegionsList(): array
    {
        return Region::all(['id','name'])->sortBy('name')->toArray();
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
    public function scopeRoots($query)
    {
        return $query->where('parent_id', null);
    }

    static function getTree(int $parentId = null, int $selectedNodeId = null)
    {
        if(is_null($parentId)){
            $regions = Region::whereNull('parent_id')->get(['id','name']);
        }else{
            $regions = Region::where('parent_id', $parentId)->get(['id','name']);
        }
        $data = [];

        //formate array with labels
        foreach ($regions as $node)
        {
            if($node->children->count() > 0){
                $row = [
                    'text' => $node->name,
                    'children'=> self::getTree($node->id, $selectedNodeId),
                    'id' => $node->id,
                ];
                if($selectedNodeId == $node->id){
                    $row['state'] = ['selected'=>true];
                }
                $data[] = $row;
            }else{
                $row = ['text' => $node->name,'id' => $node->id];
                if($selectedNodeId == $node->id){
                    $row['state'] = ['selected'=>true];
                }
                $data[] = $row;
            }
        }
        return $data;
    }
}
