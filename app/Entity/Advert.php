<?php

namespace App\Entity;

use Carbon\Carbon;
use Doctrine\DBAL\Query\QueryBuilder;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property string $status
 * @property string $content
 * @property integer $category_advert_id
 * @property integer $region_id
 * @property integer $author_id
 * @property string $reject_reason
 * @property integer $price;
 * @property string $address
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $published_at
 * @property Carbon $expires_at
 */

class Advert extends Model
{
    const STATUS_DRAFT = 'draft';
    const STATUS_MODERATION = 'moderation';
    const STATUS_ACTIVE = 'active';
    const STATUS_CLOSED = 'closed';

    //for create advert js param
    const JS_FORM_TYPE_CREATE = "create";
    //for update advert js param
    const JS_FORM_TYPE_UPDATE = "update";

    const STATUSES_LABELS = [
        self::STATUS_DRAFT => 'Drafted',
        self::STATUS_MODERATION => 'On moderation',
        self::STATUS_ACTIVE => 'Active',
        self::STATUS_CLOSED => 'Closed',
    ];

    protected $table = 'advert';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    protected $casts = [
        'published_at' => 'datetime',
        'expires_at' => 'datetime',
    ];

    public function category()
    {
        return $this->belongsTo(CategoryAdvert::class, 'category_advert_id', 'id');
    }

    public function regions()
    {
        return $this->belongsTo(Region::class, 'region_id', 'id');
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'author_id', 'id');
    }

    public function photos()
    {
        return $this->hasMany(PhotoAdvert::class, 'advert_id', 'id');
    }


    public function favorites()
    {
        return $this->belongsToMany(Advert::class, 'adverts_favorite', 'advert_id', 'user_id');
    }

    public function comments()
    {
        //show only accepted comments, after moderation
        return $this->hasMany(Comment::class)->where('status', Comment::STATUS_ACCEPTED);
    }

    public function values()
    {
        return $this->hasMany(AdvertValues::class, 'advert_id', 'id');
    }

    public function scopeFavoritesByUser(Builder $query, User $user)
    {
        return $query->whereHas('favorites', function (Builder $query) use ($user){
            return $query->where('user_id', $user->id);
        });
    }

    /**
     * Scope a query to only include active users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeByAuthor($query, User $user)
    {
        return $query->where('author_id', $user->id);
    }

    /**
     * Scope a query to only include active users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeByActive($query)
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }

    /**
     * Scope a query to only include active users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeByCategory($query, CategoryAdvert $category)
    {
        return $query->whereIn('category_advert_id', array_merge(
            [$category->id],
            $category->descendants()->pluck('id')->toArray()
        ));
    }

    public function scopeByRegion($query, Region $region)
    {
        $ids = [$region->id];
        $childrenIds = $ids;
        while ($childrenIds = Region::where(['parent_id' => $childrenIds])->pluck('id')->toArray()) {
            $ids = array_merge($ids, $childrenIds);
        }
        return $query->whereIn('region_id', $ids);
    }

    public function isDraft(): bool
    {
        return $this->status === self::STATUS_DRAFT;
    }

    public function isModeration(): bool
    {
        return $this->status === self::STATUS_MODERATION;
    }

    public function isActive(): bool
    {
        return $this->status === self::STATUS_ACTIVE;
    }

    public function isClosed(): bool
    {
        return $this->status === self::STATUS_CLOSED;
    }

    static function getStatusList(): array
    {
        return [
            self::STATUS_CLOSED,
            self::STATUS_ACTIVE,
            self::STATUS_MODERATION,
            self::STATUS_DRAFT,
        ];
    }

    public function sendToModeration(): void
    {
        if (!$this->isDraft()) {
            throw new \DomainException('Advert is not draft.');
        }
        if (!\count($this->photos)) {
            throw new \DomainException('Upload photos.');
        }
        $this->update([
            'status' => self::STATUS_MODERATION,
        ]);
    }

    public function expire(): void
    {
        $this->update([
            'status' => self::STATUS_CLOSED,
        ]);
    }

    public function close(): void
    {
        $this->update([
            'status' => self::STATUS_CLOSED,
        ]);
    }

    static function getLabelStatus(string $status): string
    {
        if(in_array ($status, self::getStatusList ()) && in_array ($status, self::STATUSES_LABELS)){
            return self::STATUSES_LABELS[$status];
        }
        return 'Undefined status - '.$status;
    }

    public function getValue($id)
    {
        foreach ($this->values as $value) {
            if ($value->attribute_id === $id) {
                return $value->value;
            }
        }
        return null;
    }
}
