<?php

namespace App\Entity;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $user_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $subject
 * @property string $message
 * @property string $status
 *
 * @method Builder forUser(User $user)
 */
class Ticket extends Model
{
    const STATUS_OPEN = 'Open';
    const STATUS_CLOSE = 'Close';
    const STATUS_APPROVED = 'Approved';

    protected $table = 'tickets';

    protected $fillable = ['subject', 'user_id', 'message', 'status'];

    public static function getStatusList(): array
    {
        return [
            self::STATUS_OPEN,
            self::STATUS_APPROVED,
            self::STATUS_CLOSE,
        ];
    }

    public function scopeByAuthor($query, User $user)
    {
        return $query->where('user_id', $user->id);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function messages()
    {
        return $this->hasMany(TicketMessage::class, 'ticket_id', 'id');
    }

    public function addMessage(int $userId, string $message): TicketMessage
    {
        if (!$this->allowsMessages()) {
            throw new \DomainException('Ticket is closed for messages.');
        }
        $message = $this->messages()->create([
            'user_id' => $userId,
            'message' => $message,
        ]);

        return $message;
    }

    public function allowsMessages(): bool
    {
        return !$this->isClosed();
    }

    private function setStatus($status): void
    {
        $this->update(['status' => $status]);
    }

    public function approve(): void
    {
        if ($this->isApproved()) {
            throw new \DomainException('Ticket is already approved.');
        }
        $this->setStatus(Ticket::STATUS_APPROVED);
    }

    public function close(): void
    {
        if ($this->isClosed()) {
            throw new \DomainException('Ticket is already closed.');
        }
        $this->setStatus(Ticket::STATUS_CLOSE);
    }

    public function reopen(): void
    {
        if (!$this->isClosed()) {
            throw new \DomainException('Ticket is not closed.');
        }
        $this->setStatus(Ticket::STATUS_APPROVED);
    }

    public function isOpen(): bool
    {
        return $this->status === Ticket::STATUS_OPEN;
    }

    public function isApproved(): bool
    {
        return $this->status === Ticket::STATUS_APPROVED;
    }

    public function isClosed(): bool
    {
        return $this->status === Ticket::STATUS_CLOSE;
    }

    public function canBeRemoved(): bool
    {
        return $this->isOpen();
    }
}
