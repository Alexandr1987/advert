<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    const STATUS_DRAFT = 0;
    const STATUS_ACCEPTED = 1;
    const STATUS_BLOCKED = 2;

    const LABEL_FOR_STATUS = [
        self::STATUS_BLOCKED => 'Blocked',
        self::STATUS_ACCEPTED => 'Accepted',
        self::STATUS_DRAFT => 'Draft',
    ];

    protected $table = 'comments';

    protected $fillable = ['user_id', 'advert_id', 'status', 'message'];

    public function isDraft(): bool
    {
        return $this->status === self::STATUS_DRAFT;
    }

    public function isAccepted(): bool
    {
        return $this->status === self::STATUS_ACCEPTED;
    }

    public function isBlocked(): bool
    {
        return $this->status === self::STATUS_BLOCKED;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function advert()
    {
        return $this->belongsTo(Advert::class);
    }

    static function getStatusList(): array
    {
        return [
            self::STATUS_DRAFT,
            self::STATUS_ACCEPTED,
            self::STATUS_BLOCKED,
        ];
    }

    static public function getLabelStatusInfo(string $status): string
    {
        $list = self::getStatusList();

        if(in_array($status, $list)){
            return self::LABEL_FOR_STATUS[$status];
        }
        return 'Not find label';
    }

    public function getStatusLabel()
    {
        return self::getLabelStatusInfo($this->status);
    }
}
