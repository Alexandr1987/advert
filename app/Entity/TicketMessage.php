<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

/**
 * @property int $id
 * @property int $user_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $message
 */
class TicketMessage extends Model
{
    protected $table = 'tickets_messages';

    protected $fillable = ['ticket_id', 'user_id', 'message'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function renderOneMessage(): string
    {
        return view('ticket.onemessage', ['created_at' => $this->created_at, 'message'=> $this->message, 'name'=>$this->user->name])->render ();
    }
}