<?php


namespace App\Service;

use App\Entity\Advert;
use App\Entity\CategoryAdvert;
use App\Entity\Region;
use App\Http\Requests\Admin\Ticket\SearchRequest;
use App\Http\Requests\Search\SearchAdvertRequest;
use Elasticsearch\Client;
use Illuminate\Database\Query\Expression;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;

class AdvertSearchService
{
    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    protected function getSearchRules(?int $categoryId, ?int $regionId, SearchAdvertRequest $searchAdvertRequest): array
    {
        //make array rules for search in ES
        $rules = [];

        //only active adverts
        //$rules[] = ['term' => ['status' => Advert::STATUS_ACTIVE]];

        //use filter by category Id
        if ($categoryId !== null) {
            $rules[] = ['term' => ['category' => $categoryId]];
        }

        //use filter by region Id
        if ($regionId !== null) {
            $rules[] = ['term' => ['regions' => $regionId]];
        }

        //if exist text param - used
        if (!empty($searchAdvertRequest['text'])) {
            //use multi_match for find by multi fields
            $rules[] = [
                'multi_match' => [
                    //word for search
                    'query' => $searchAdvertRequest['text'],
                    //search in fields in advert ES document(title, content)
                    'fields' => ['title^3', 'content']
                ]
            ];
        }
        return $rules;
    }

    public function search(?CategoryAdvert $category, ?Region $region, SearchAdvertRequest $request, int $perPage, int $page): array
    {
        try{
            //send request o ES
            $response = $this->client->search([
                'index' => AdvertIndexElasticSearchService::NAME_INDEX,
                'type' => AdvertIndexElasticSearchService::NAME_TYPE,
                'body' => [
                    //get from ES only ids adverts
                    '_source' => ['id'],
                    'from' => ($page - 1) * $perPage,
                    'size' => $perPage,
                    //use group by region and category
                    //for counts in groups, category
                    'aggs' => [
                        'group_by_region' => [
                            'terms' => [
                                'field' => 'regions',
                            ],
                        ],
                        'group_by_category' => [
                            'terms' => [
                                'field' => 'category',
                            ],
                        ],
                    ],
                    'query' => [
                        'bool' => [
                            //get array rules, all get rules accept  - apply simultaneously(because param - "must")
                            'must' => $this->getSearchRules($category->id, $region->id, $request)
                        ],
                    ],
                ],
            ]);
            $totalFindAdverts = $response['hits']['total'];
        }catch (\Exception $exception){
            $totalFindAdverts = 0;
        }

        $items = [];

        $groupRegionCounts = [];
        $groupCategoriesCounts = [];

        //get only advert ids from ES
        if($totalFindAdverts > 0){
            $ids = array_column($response['hits']['hits'], '_id');

            //get counts by regions
            $groupRegionCounts = array_column($response['aggregations']['group_by_region']['buckets'], 'doc_count', 'key');

            //get counts by categories
            $groupCategoriesCounts = array_column($response['aggregations']['group_by_category']['buckets'], 'doc_count', 'key');

            if ($ids) {
                $items = Advert::with(['category', 'regions'])
                    ->whereIn('id', $ids)
                    //after get ids from ES with order we accept same order in MYSQL
                    ->orderBy(new Expression('FIELD(id,' . implode(',', $ids) . ')'))
                    ->get();
                $pagination = new LengthAwarePaginator($items, $totalFindAdverts, $perPage, $page);
            }
        }else{
            $pagination = $this->getEmptyPaginator($perPage, $page);
        }

        //prepare result array
        return [
            'adverts' => $pagination,
            'groupRegionCounts' => $groupRegionCounts,
            'groupCategoriesCounts' => $groupCategoriesCounts,
        ];
    }

    protected function getEmptyPaginator(int $perPage, int $page)
    {
        return new LengthAwarePaginator([], 0, $perPage, $page);
    }
}