<?php

namespace App\Service;

use App\Entity\Ticket;
use App\Entity\TicketMessage;
use App\Events\Ticket\CreateMessage;
use App\Events\Ticket\UserCreate;
use App\Http\Requests\Admin\Ticket\SearchRequest;
use App\Http\Requests\Cabinet\Ticket\CreateRequest;
use App\Http\Requests\Cabinet\Ticket\MessageRequest;
use App\Http\Requests\Cabinet\Ticket\UpdateRequest;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;

class TicketService
{
    public function create(int $userId, CreateRequest $request): Ticket
    {
        $ticket = Ticket::create([
            'user_id' => $userId,
            'subject' => $request['subject'],
            'message' => $request['message'],
            'status' => Ticket::STATUS_OPEN,
        ]);

        //exec event for admin
        event(new UserCreate($ticket));

        return $ticket;
    }

    public function edit(int $id, UpdateRequest $request): void
    {
        $ticket = $this->loadTicket($id);

        $ticket->update(
            $request['subject'],
            $request['message']
        );
    }

    public function renderMessagesByTicket(Ticket $ticket): array
    {
        $messages = [];

        $ticket->messages()->orderBy('id')->with('user')->each(function (TicketMessage $ticketMessage, $key) use (&$messages) {
            $messages[] = $ticketMessage->renderOneMessage();
        });

        return $messages;
    }

    public function findAll(SearchRequest $request)
    {
        $query = Ticket::orderByDesc('updated_at');

        //only admin can filtered by params
        if(Auth::user()->isRoleAdmin() || Auth::user()->isRoleAdmin()){
            if (!empty($value = $request->get('id'))) {
                $query->where('id', $value);
            }
            if (!empty($value = $request->get('user'))) {
                $query->where('user_id', $value);
            }
            if (!empty($value = $request->get('status'))) {
                $query->where('status', $value);
            }
        }else{
            //not admin - filtered by current user
            $query->where('user_id', Auth::user()->id);
        }

        return $query->paginate(20);
    }

    protected function loadTicket(int $ticketId): Ticket
    {
        return Ticket::findOrFail($ticketId);
    }

    public function removeByOwner(int $id): void
    {
        $ticket = $this->loadTicket($id);
        if (!$ticket->canBeRemoved()) {
            throw new \DomainException('Unable to remove active ticket');
        }
        $ticket->delete();
    }

    public function removeByAdmin(int $id): void
    {
        $ticket = $this->loadTicket($id);
        $ticket->delete();
    }

    public function message(int $userId, int $id, MessageRequest $request): void
    {
        $ticket = $this->loadTicket($id);

        $message = $ticket->addMessage($userId, $request['message']);

        broadcast(new CreateMessage($id, $message));
    }

    public function approve(int $userId, int $id): void
    {
        $ticket = $this->loadTicket($id);
        $ticket->approve($userId);
    }

    public function close(int $userId, int $id): void
    {
        $ticket = $this->loadTicket($id);
        $ticket->close($userId);
    }

    public function reopen(int $userId, int $id): void
    {
        $ticket = $this->loadTicket($id);
        $ticket->reopen($userId);
    }
}