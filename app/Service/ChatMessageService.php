<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 17.01.19
 * Time: 22:35
 */

namespace App\Service;

use App\Entity\Advert;
use App\Entity\ChatMessage;
use App\Entity\User;
use App\Http\Requests\Cabinet\ChatMessage\SendMessageRequest;

class ChatMessageService
{
    public function sendMessageOnAdvert(int $userId, int $advertId, string $message, SendMessageRequest $request)
    {
        /** @var User $user */
        $user = User::findOrFail ($userId);

        /** @var  $advert */
        $advert = Advert::findOrFail ($advertId);

        /** @var ChatMessage $chatMessage */
        $chatMessage = ChatMessage::make([
            'user_id' => $user->id,
            'message' => $request['message'],
            'advert_id' => $advert->id,
        ]);
    }
}