<?php


namespace App\Service;

use App\Entity\Comment;
use App\Events\Comment\Create as CommentCreate;
use App\Http\Requests\Cabinet\Comment\CreateRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class CommentService
{
    public function create(int $advertId, int $userId, CreateRequest $request)
    {
        $comment = Comment::make([
            'user_id' => $userId,
            'advert_id' => $advertId,
            'message' => $request['message'],
            'status' => Comment::STATUS_DRAFT,
        ]);

        //save in db
        if($comment->saveOrFail()){
            //run event fro admin or manager user
            //notify moderation users
            event(new CommentCreate());
        }else{
            throw new ModelNotFoundException('Not create Comment model');
        }
    }

    public function getList(Request $request)
    {
        //TODO add filter from params $request params
        $query = Comment::with('advert')->orderBy('id', 'desc');

        //get users with pagination
        return  $query->paginate(20);
    }

    public function delete(int $commentId)
    {
        $comment = $this->loadComment($commentId);

        return $comment->update(['status'=>Comment::STATUS_BLOCKED]);
    }

    public function accepted(int $commentId)
    {
        $comment = $this->loadComment($commentId);

        return $comment->update(['status'=>Comment::STATUS_ACCEPTED]);
    }

    protected function loadComment(int $commentId): Comment
    {
        return Comment::findOrFail($commentId);
    }
}