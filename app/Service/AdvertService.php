<?php

namespace App\Service;

use App\Entity\Advert;
use App\Entity\PhotoAdvert;
use App\Entity\Region;
use App\Entity\User;
use App\Events\Advert\ModerationPassed;
use App\Events\Advert\UpdateAdvert;
use App\Http\Requests\Cabinet\Advert\CreateRequest as AdvertCreateRequest;
use App\Entity\CategoryAdvert;
use App\Http\Requests\Cabinet\Advert\CreateRequest;
use App\Http\Requests\Cabinet\Photo\DeleteRequest;
use App\Jobs\Advert\DeletePhoto;
use App\Jobs\Advert\UploadPhoto;
use App\Notifications\Advert\ModerationPassedNotification;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\Cabinet\Photo\CreateRequest as PhotoCreateRequest;
use Illuminate\Support\Facades\Storage;

class AdvertService
{
    public function create(int $userId, $regionId, int $categoryId, AdvertCreateRequest $request): Advert
    {
        //find category for advert
        /** @var CategoryAdvert $category */
        $category = CategoryAdvert::findOrFail($categoryId);

        //find region for advert
        if ($regionId !== null) {
            /** @var Region $region */
            $region = Region::findOrFail($regionId);
        }

        //find user - author advert
        /** @var User $user */
        $user = User::findOrFail($userId);

        DB::beginTransaction();

        /** @var Advert $advert */
        $advert = Advert::make([
            'title' => $request['title'],
            'content' => $request['content'],
            'price' => $request['price'],
            'address' => $request['address'],
            'status' => Advert::STATUS_DRAFT,
            'slug' => str_slug($request['title']),
        ]);

        //add relation to advert from others models
        $advert->author()->associate($user);
        $advert->category()->associate($category);
        $advert->regions()->associate($region);

        //save in db
        if($advert->saveOrFail()){
            //save eav-attributes
            $this->updateAttributesValues($advert, $request);
            DB::commit();
            return $advert;
        }else{
            DB::rollBack();
            throw new ModelNotFoundException('Not create Advert model');
        }
    }

    public function update(int $advertId, CreateRequest $request): void
    {
        /** @var Advert $advert */
        $advert = $this->findAdvert($advertId);

        //get old price
        $oldPrice = $advert->price;

        DB::beginTransaction();

        $advert->update($request->only([
            'title',
            'content',
            'price',
            'address',
        ]));

        //TODO add event to update price in advert + queue for sending emails(for users , witch add in favorite advert)
        //remove exist attributes values
        $advert->values()->delete();

        //update attributes
        $this->updateAttributesValues($advert, $request);

        //fix update
        if($advert->save()){
            DB::commit();
            //if price in advert is changed - exec event
            if($oldPrice!=$advert->price){
                event(new UpdateAdvert($advert));
            }
        }else{
            DB::rollBack();
            throw new ModelNotFoundException('Can not update ADvert model');
        }
    }

    protected function updateAttributesValues(Advert $advert, CreateRequest $request): void
    {
        //add values
        foreach ($advert->category->allAttributes() as $attribute) {
            $value = $request['attribute_'.$attribute->id] ?? null;
            if (!empty($value)) {
                $advert->values()->create([
                    'attribute_id' => $attribute->id,
                    'value' => $value,
                ]);
            }
        }
    }

    public function remove(int $id): void
    {
        /** @var Advert $advert */
        $advert = $this->findAdvert($id);
        $advert->delete();
    }

    public function addPhoto(int $advertId, PhotoCreateRequest $request): void
    {
        //upload file on server
        $path = Storage::disk('local')->putFile('adverts/'.$advertId, $request->file('photo'));

        //write in db info about uploaded file and update status
        $model = PhotoAdvert::create(
            [
                'advert_id' => $advertId,
                //path on disk storage
                'file' => $path,
                //public link to file
                'public_full_link' => '',
                //source storage for file upload
                'storage' => 'dropbox',
                //file on server - add to queue and upload to clound
                'status' => PhotoAdvert::STATUS_CREATED
            ]
        );

        //add file to queue to upload file in clound
        UploadPhoto::dispatch($path, $model->id);
    }

    public function sendToModeration(int $advertId)
    {
        $advert = $this->findAdvert($advertId);

        $advert->sendToModeration();

        //send notify for user, change status of advert
        $advert->user->notify(new ModerationPassedNotification($advert));

        event(new ModerationPassed($advert));
    }

    public function close(int $advertId)
    {
        $advert = $this->findAdvert($advertId);

        $advert->close();
    }

    public function expire(Advert $advert): void
    {
        $advert->expire();
    }

    private function findAdvert(int $advertId): Advert
    {
        return Advert::findOrFail($advertId);
    }

    public function deletePhoto(DeleteRequest $request, $advertId)
    {
        $advert = $this->findAdvert($advertId);

        $photo = $advert->photos()->where(['id'=>$request['file']])->first ();

        //delete file from clound in queue
        DeletePhoto::dispatch($photo->file);

        $photo->delete ();
    }
}