<?php

namespace App\Service;

use App\Entity\Advert;
use App\Entity\User;
use Exception;

class FavoriteService
{
    public function attachToUser(int $userId, int $advertId): void
    {
        $advert = $this->findAdvert($advertId);

        $user = $this->findUser($userId);

        $existFavorite = $user->favorites()->where('id', $advert->id)->exists();

        if($existFavorite){
            throw new Exception('This advert already exist in favorite');
        }

        $user->favorites()->attach($advert->id);
    }

    public function detachToUser(int $userId, int $advertId)
    {
        $advert = $this->findAdvert($advertId);

        $user = $this->findUser($userId);

        $user->favorites()->detach($advert->id);
    }

    protected function findAdvert(int $advertId): Advert
    {
        return Advert::findOrFail($advertId);
    }

    protected function findUser(int $userId): User
    {
        return User::findOrFail($userId);
    }
}