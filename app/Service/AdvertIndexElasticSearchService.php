<?php


namespace App\Service;

use App\Entity\Advert;
use Elasticsearch\Client;

class AdvertIndexElasticSearchService
{
    const NAME_INDEX = 'adverts';
    const NAME_TYPE = 'advert';

    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function clear(): void
    {
        $this->client->deleteByQuery([
            'index' => self::NAME_INDEX,
            'type' => self::NAME_TYPE,
            'body' => [
                'query' => [
                    'match_all' => new \stdClass(),
                ],
            ],
        ]);
    }

    public function index(Advert $advert): void
    {
        $regions = [];
        if ($region = $advert->region) {
            do {
                $regions[] = $region->id;
            } while ($region = $region->parent);
        }
        $this->client->index([
            'index' => self::NAME_INDEX,
            'type' => self::NAME_TYPE,
            'id' => $advert->id,
            'body' => [
                'id' => $advert->id,
                'published_at' => $advert->published_at ? $advert->published_at->format(DATE_ATOM) : null,
                'title' => $advert->title,
                'content' => $advert->content,
                'price' => $advert->price,
                'status' => $advert->status,
                'category' => array_merge(
                    [$advert->category->id],
                    $advert->category->ancestors()->pluck('id')->toArray()
                ),
                'regions' => $regions ?: [0],
                //TODO add EAV values from relation (values)
            ],
        ]);
    }

    public function remove(Advert $advert): void
    {
        $this->client->delete([
            'index' => self::NAME_INDEX,
            'type' => self::NAME_TYPE,
            'id' => $advert->id,
        ]);
    }
}