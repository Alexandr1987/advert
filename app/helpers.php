<?php

use App\Entity\Region;
use App\Http\Router\AdvertsPath;
use App\Entity\CategoryAdvert;

if (! function_exists('adverts_path')) {
    function adverts_path(?Region $region, ?CategoryAdvert $category)
    {
        return app()->make(AdvertsPath::class)
            ->withRegion($region)
            ->withCategory($category);
    }
}