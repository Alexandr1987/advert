<?php

namespace App\Listeners\Advert;

use App\Notifications\Advert\ModerationPassedNotification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\Advert\ModerationPassed as ModerationPassedEvent;

class ModerationPassed
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(ModerationPassedEvent $event)
    {
        $advert = $event->advert;

        //send notify to user
        $advert->user->notify(new ModerationPassedNotification($advert));
    }
}
