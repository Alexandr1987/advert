<?php

namespace App\Listeners\Advert;

use App\Entity\User;
use App\Events\Advert\UpdateAdvert;
use App\Notifications\Advert\UpdatePriceNotification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdatePrice
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UpdateAdvert $event)
    {
        $advert = $event->advert;

        //get all users witch add advert to favorites
        /** @var User $user */
        foreach ($advert->favorites()->cursor() as $user) {
            //send user notify, via email
            $user->notify(new UpdatePriceNotification($advert));
        }
    }
}
