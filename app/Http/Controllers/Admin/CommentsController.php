<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Comment;
use App\Service\CommentService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommentsController extends Controller
{
    protected $commentService;

    public function __construct(CommentService $commentService)
    {
        $this->commentService = $commentService;
    }

    public function index(Request $request)
    {
        $comments = $this->commentService->getList($request);

        //list statuses of users
        $statuses = Comment::getStatusList();

        //get list users
        return view('admin.comments.list', compact('comments', 'statuses'));
    }

    public function show(Comment $comment)
    {
        return view('admin.comments.show', compact('comment'));
    }

    public function delete(int $commentId)
    {
        $this->commentService->delete($commentId);

        return back();
    }

    public function accepted(int $commentId)
    {
        $this->commentService->accepted($commentId);

        return back();
    }
}
