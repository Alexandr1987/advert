<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\Region\CreateRequest;
use App\Http\Requests\Admin\Region\UpdateRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entity\Region;

class RegionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Region::where('parent_id', null)->orderBy('name', 'asc');

        //get regions with pagination
        $regions = $query->paginate(30);

        //get list 
        return view('admin.regions.list', compact('regions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $parent = null;

        if(!empty($request->get('parent'))){
            $parent = Region::findOrFail($request->get('parent'));
        }

        //form for create user
        return view('admin.regions.create', compact('parent'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        $region = Region::create([
            'name' => $request->get('name'),
            'slug' => $request->get('slug'),
            'parent_id' => $request->get('parent_id'),
        ]);

        return redirect()->route('admin.regions.show', $region);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Region $region)
    {
        $regions = $region->children ()->orderBy ('name')->paginate (20);

        return view('admin.regions.show', compact('region', 'regions'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Region $region)
    {
        return view('admin.regions.edit', compact('region'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Region $region)
    {
        //update after validation data
        $region->update(
            $request->only(
                ['name', 'slug', 'parent_id']
            )
        );

        return redirect()->route('admin.regions.show', compact('region'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Region $region)
    {
        $region->delete();

        return redirect()->route('admin.regions.index');
    }
}
