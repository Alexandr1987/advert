<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\CategoryAdvertAttributes\CreateRequest;
use App\Http\Requests\Admin\CategoryAdvertAttributes\UpdateRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entity\CategoryAdvert;
use App\Entity\CategoryAdvertAttributes;

class CategoryAdvertAttributesController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(CategoryAdvert $category)
    {
        $types = CategoryAdvertAttributes::getTypeList();

        return view('admin.category_advert_attributes.create', compact('category', 'types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request, CategoryAdvert $category)
    {
        $attribute = $category->attributes()->create([
            'name' => $request['name'],
            'type' => $request['type'],
            'required' => (bool)$request['required'],
            'variants' => array_map('trim', preg_split('#[\r\n]+#', $request['variants'])),
            'sort' => $request['sort'],
        ]);
        return redirect()->route('admin.category-advert.attributes.show', [$category, $attribute]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(CategoryAdvert $category, CategoryAdvertAttributes $attribute)
    {
        return view('admin.category_advert_attributes.list', compact('category', 'attribute'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(CategoryAdvert $category, CategoryAdvertAttributes $attribute)
    {
        $types = CategoryAdvertAttributes::getTypeList();

        return view('admin.category_advert_attributes.edit', compact('category', 'attribute', 'types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, CategoryAdvert $category, CategoryAdvertAttributes $attribute)
    {
        $category->attributes()->findOrFail($attribute->id)->update([
            'name' => $request['name'],
            'type' => $request['type'],
            'required' => (bool)$request['required'],
            'variants' => array_map('trim', preg_split('#[\r\n]+#', $request['variants'])),
            'sort' => $request['sort'],
        ]);
        return redirect()->route('admin.category-advert.show', $category);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(CategoryAdvert $categoryAdvert)
    {
        $categoryAdvert->delete();

        return redirect()->route('admin.category-advert.show', $categoryAdvert);
    }
}
