<?php

namespace App\Http\Controllers\Admin;

use App\Entity\CategoryAdvert;
use App\Http\Requests\Admin\CategoryAdvert\CreateRequest;
use App\Http\Requests\Admin\CategoryAdvert\UpdateRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoryAdvertController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //get list categories
        $categories = CategoryAdvert::defaultOrder()->withDepth()->get();

        return view('admin.category_advert.list', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $parents = CategoryAdvert::defaultOrder()->withDepth()->get();

        return view('admin.category_advert.create', compact('parents', 'request'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        $categoryAdvert = CategoryAdvert::create([
            'name' => $request->get('name'),
            'slug' => str_slug($request->get('name')),
            'parent_id' => $request->get('parent'),
        ]);

        return redirect()->route('admin.category-advert.show', ['category'=>$categoryAdvert]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entity\CategoryAdvert  $categoryAdvert
     * @return \Illuminate\Http\Response
     */
    public function show(CategoryAdvert $categoryAdvert)
    {
        //attributes for parent category
        $parentAttributes = $categoryAdvert->parentAttributes();

        //attributes for current category
        $attributes = $categoryAdvert->attributes;

        return view('admin.category_advert.show', ['category'=> $categoryAdvert, 'parentAttributes' => $parentAttributes, 'attributes'=> $attributes]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entity\CategoryAdvert  $categoryAdvert
     * @return \Illuminate\Http\Response
     */
    public function edit(CategoryAdvert $categoryAdvert)
    {
        $parents = CategoryAdvert::defaultOrder()->withDepth()->get();

        return view('admin.category_advert.edit', ['category'=>$categoryAdvert, 'parents'=>$parents]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entity\CategoryAdvert  $categoryAdvert
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, CategoryAdvert $categoryAdvert)
    {
        $categoryAdvert->update([
            'name' => $request->get('name'),
            'slug' => str_slug($request->get('name')),
            'parent_id' => $request->get('parent'),
        ]);
        return redirect()->route('admin.category-advert.show', $categoryAdvert);
    }

    public function first(CategoryAdvert $category)
    {
        if ($first = $category->siblings()->defaultOrder()->first()) {
            $category->insertBeforeNode($first);
        }
        return redirect()->route('admin.category-advert.index');
    }
    public function up(CategoryAdvert $category)
    {
        $category->up();
        return redirect()->route('admin.category-advert.index');
    }
    public function down(CategoryAdvert $category)
    {
        $category->down();
        return redirect()->route('admin.category-advert.index');
    }
    public function last(CategoryAdvert $category)
    {
        if ($last = $category->siblings()->defaultOrder('desc')->first()) {
            $category->insertAfterNode($last);
        }
        return redirect()->route('admin.category-advert.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entity\CategoryAdvert  $categoryAdvert
     * @return \Illuminate\Http\Response
     */
    public function destroy(CategoryAdvert $categoryAdvert)
    {
        $categoryAdvert->delete();

        return redirect()->route('admin.category-advert.index');
    }
}
