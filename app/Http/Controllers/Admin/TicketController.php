<?php

namespace App\Http\Controllers\Admin;

use App\Entity\TicketMessage;
use App\Http\Requests\Admin\Ticket\SearchRequest;
use App\Http\Requests\Cabinet\Ticket\MessageRequest;
use App\Http\Requests\Cabinet\Ticket\UpdateRequest;
use App\Service\TicketService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entity\Ticket;
use Illuminate\Support\Facades\Auth;

class TicketController extends Controller
{
    protected $ticketService;

    public function __construct(TicketService $ticketService)
    {
        $this->ticketService = $ticketService;
    }

    public function index(SearchRequest $request)
    {
        $tickets = $this->ticketService->findAll($request);

        $statuses = Ticket::getStatusList();

        return view('admin.tickets.list', compact('tickets', 'statuses'));
    }

    public function show(Ticket $ticket)
    {
        //convert to json, for vuejs component
        $messages = json_encode($this->ticketService->renderMessagesByTicket($ticket));

        return view('admin.tickets.show', compact('ticket','messages'));
    }
    public function editForm(Ticket $ticket)
    {
        return view('admin.tickets.edit', compact('ticket'));
    }
    
    public function edit(UpdateRequest $request, Ticket $ticket)
    {
        try {
            $this->ticketService->edit($ticket->id, $request);
        } catch (\DomainException $e) {
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('admin.tickets.show', $ticket);
    }
    
    public function message(MessageRequest $request, Ticket $ticket)
    {
        try {
            $this->ticketService->message(Auth::id(), $ticket->id, $request);
        } catch (\DomainException $e) {
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('cabinet.tickets.show', $ticket);
    }
    
    public function approve(Ticket $ticket)
    {
        try {
            $this->ticketService->approve(Auth::id(), $ticket->id);
        } catch (\DomainException $e) {
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('admin.tickets.show', $ticket);
    }
    
    public function close(Ticket $ticket)
    {
        try {
            $this->ticketService->close(Auth::id(), $ticket->id);
        } catch (\DomainException $e) {
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('admin.tickets.show', $ticket);
    }
    
    public function reopen(Ticket $ticket)
    {
        try {
            $this->ticketService->reopen(Auth::id(), $ticket->id);
        } catch (\DomainException $e) {
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('admin.tickets.show', $ticket);
    }
    
    public function destroy(Ticket $ticket)
    {
        try {
            $this->ticketService->removeByAdmin($ticket->id);
        } catch (\DomainException $e) {
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('admin.tickets.index');
    }    
}
