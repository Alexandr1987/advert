<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Advert;
use App\Entity\CategoryAdvert;
use App\Entity\Region;
use App\Service\AdvertService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;

class AdvertController extends Controller
{
    protected $advertService;

    public function __construct(AdvertService $advertService)
    {
        /** @var AdvertService advertService */
        $this->advertService = $advertService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //filtered adverts by author
        $adverts = Advert::with (['category', 'regions','author'])
            ->orderBy('created_at', 'DESC')
            ->paginate(50);

        return view('admin.advert.list', compact('adverts'));
    }

    public function send(Advert $advert)
    {

        try {
            $this->advertService->sendToModeration($advert->id);
        } catch (\DomainException $e) {
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('adverts.show', $advert);
    }

    public function close(Advert $advert)
    {
        if(!Gate::allows('owner-advert', $advert)){
            abort(403);
        }

        try {
            $this->advertService->close($advert->id);
        } catch (\DomainException $e) {
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('adverts.show', $advert);
    }
}
