<?php

namespace App\Http\Controllers\Advert;

use App\Entity\Advert;
use App\Entity\CategoryAdvert;
use App\Entity\Region;
use App\Http\Controllers\Controller;
use App\Http\Requests\Cabinet\Comment\CreateRequest;
use App\Http\Requests\Search\SearchAdvertRequest;
use App\Http\Router\AdvertsPath;
use App\Service\AdvertSearchService;
use App\Service\CommentService;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Exception;

class AdvertController extends Controller
{
    protected $searchService;
    protected $commentService;

    public function __construct(AdvertSearchService $advertSearchService, CommentService $commentService)
    {
        $this->searchService = $advertSearchService;
        $this->commentService = $commentService;
    }

    public function index(SearchAdvertRequest $request, AdvertsPath $path)
    {
        $region = $path->region;
        $category = $path->category;
        $result = $this->searchService->search($category, $region, $request, 20, $request->get('page', 1));

        $adverts = $result['adverts'];
        $regionsCounts = $result['groupCategoriesCounts'];
        $categoriesCounts = $result['groupRegionCounts'];


        $query = $region ? $region->children() : Region::roots();
        $regions = $query->orderBy('name')->getModels();
        $query = $category ? $category->children() : CategoryAdvert::whereIsRoot();
        $categories = $query->defaultOrder()->getModels();


        //filtered regions without adverts
//        $regions = array_filter($regions, function (Region $region) use ($regionsCounts) {
//            return isset($regionsCounts[$region->id]) && $regionsCounts[$region->id] > 0;
//        });

        //filtered category without adverts
//        $categories = array_filter($categories, function (CategoryAdvert $category) use ($categoriesCounts) {
//            return isset($categoriesCounts[$category->id]) && $categoriesCounts[$category->id] > 0;
//        });

        return view('advert.index', compact(
            'category', 'region',
            'categories', 'regions',
            'regionsCounts', 'categoriesCounts',
            'adverts'
        ));
    }

    public function phone(Advert $advert)
    {
        //TODO get phone author advert(user click on btn - and get to show phone number)
        return $advert->author->phone;
    }

    public function show(Advert $advert)
    {
        //if (!$advert->isActive()) {abort(403);}

        $user = Auth::user();

        return view('advert.show', compact('advert', 'user'));
    }

    public function addComment(CreateRequest $request, Advert $advert)
    {
        //user send comment for advert
        try{
            $this->commentService->create($advert->id, Auth()->user()->id, $request);
            $response = ['errors'=>'', 'msg'=>'Success comment sended'];
        }catch (Exception $exception){
            $response = ['errors'=>$exception->getMessage(), 'msg'=>'Have errors'];
        }
        return $response;
    }
}