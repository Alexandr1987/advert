<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\Auth\RegisterRequest;
use App\Mail\Auth\VerifyEmail;
use App\Entity\User;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Entity\User
     */
    protected function create(array $data)
    {
        $user = User::register ( $data['name'], $data['email'], $data['password'] );

        Mail::to ( $user->email )->send ( new VerifyEmail( $user ) );

        return $user;
    }

    public function verify($token)
    {
        $user = User::where('verify_string', $token)->first();

        //not find user by token
        if(empty($user)){
            return redirect()->route('login')->with('error', 'Not find user by token');
        }

        try{
            User::verifyUserById($user->id);
        }catch (\Exception $exception){
            return redirect()->route('login')->with('error', $exception->getMessage());
        }

        return redirect()->route('login')->with('success', 'Your e-mail is verified, your can login');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(RegisterRequest $request)
    {
        $user = $this->create($request->all());

        //event - registration user
        event(new Registered($user));

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }

    protected function registered(Request $request, $user)
    {
        //logout user,wait for confirm by email link
        $this->guard()->logout();

        return redirect()->route('login')
            ->with('success', 'Check your email and confirm registration');
    }
}
