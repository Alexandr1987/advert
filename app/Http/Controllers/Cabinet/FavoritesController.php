<?php

namespace App\Http\Controllers\Cabinet;

use App\Entity\Advert;
use App\Service\FavoriteService;
use App\Http\Controllers\Controller;
use Exception;

class FavoritesController extends Controller
{
    protected $favoriteService;

    public function __construct(FavoriteService $favoriteService)
    {
        $this->middleware('auth');

        /** @var FavoriteService  $favoriteService*/
        $this->favoriteService = $favoriteService;
    }

    public function index()
    {
        $adverts = Advert::favoritesByUser(\Auth::user())->paginate(20);

        return view('cabinet.favorites.list', compact('adverts'));
    }

    public function remove(Advert $advert)
    {
        try{
            $this->favoriteService->detachToUser(\Auth::user()->id, $advert->id);
        }catch (Exception $exception){
            return back()->with('error', $exception->getMessage());
        }
        return back()->with('success', 'Success remove advert from favorites');
    }

    public function add(Advert $advert)
    {
        try{
            $this->favoriteService->attachToUser(\Auth::user()->id, $advert->id);
        }catch (Exception $exception){
            return back()->with('error', $exception->getMessage());
        }
        return back()->with('success', 'Success add advert in favorites');
    }
}
