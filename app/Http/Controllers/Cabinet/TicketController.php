<?php

namespace App\Http\Controllers\Cabinet;

use App\Entity\Ticket;
use App\Http\Requests\Cabinet\Ticket\CreateRequest;
use App\Http\Requests\Cabinet\Ticket\MessageRequest;
use App\Service\TicketService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class TicketController extends Controller
{
    private $service;

    public function __construct(TicketService $service)
    {
        $this->service = $service;
    }
    public function index()
    {
        $tickets = Ticket::ByAuthor(Auth::user())->orderByDesc('updated_at')->paginate(20);
        return view('cabinet.tickets.index', compact('tickets'));
    }
    public function show(Ticket $ticket)
    {

        $this->checkAccess($ticket);

        //convert to json, for vuejs component
        $messages = json_encode($this->service->renderMessagesByTicket($ticket));

        return view('cabinet.tickets.show', compact('ticket', 'messages'));
    }
    public function create()
    {
        return view('cabinet.tickets.create');
    }
    public function store(CreateRequest $request)
    {
        try {
            $ticket = $this->service->create(Auth::id(), $request);
        } catch (\DomainException $e) {
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('cabinet.tickets.show', $ticket);
    }
    public function message(MessageRequest $request, Ticket $ticket)
    {
        $this->checkAccess($ticket);

        try {
            $this->service->message(Auth::id(), $ticket->id, $request);
        } catch (\DomainException $e) {
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('cabinet.tickets.show', $ticket);
    }
    public function destroy(Ticket $ticket)
    {
        $this->checkAccess($ticket);
        try {
            $this->service->removeByOwner($ticket->id);
        } catch (\DomainException $e) {
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('cabinet.favorites.index');
    }
    private function checkAccess(Ticket $ticket): void
    {
        if (!Gate::allows('owner-ticket', $ticket)) {
            abort(403);
        }
    }
}
