<?php

namespace App\Http\Controllers\Cabinet;

use App\Entity\User;
use App\Http\Requests\Cabinet\Profile\UpdateProfileRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function index()
    {
        $user = Auth::user();

        return view('cabinet.profile.index', compact('user'));
    }

    public function edit()
    {
        //get form for update profile
        $user = Auth::user();

        return view('cabinet.profile.edit', compact('user'));
    }

    public function update(UpdateProfileRequest $request)
    {
        //save - updated info in user profile
        $user = User::findOrFail(Auth::user()->id);

        $user->update($request->only('name', 'last_name', 'phone'));

        return redirect()->route('cabinet.profile.index')->with('success', 'Success update profile');
    }

    public function phone()
    {

    }
}
