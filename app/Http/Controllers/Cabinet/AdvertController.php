<?php

namespace App\Http\Controllers\Cabinet;

use App\Entity\Advert;
use App\Entity\CategoryAdvert;
use App\Entity\Region;
use App\Http\Middleware\IsFilledProfile;
use App\Http\Requests\Cabinet\Advert\CreateRequest;
use App\Service\AdvertService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Controller;
use Auth;
use App\Http\Requests\Cabinet\Photo\CreateRequest as PhotoCreateRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class AdvertController extends Controller
{
    protected $advertService;

    public function __construct(AdvertService $advertService)
    {
        $this->middleware(IsFilledProfile::class);

        /** @var AdvertService advertService */
        $this->advertService = $advertService;
    }

    public function index()
    {
        //filtered adverts by author
        $adverts = Advert::with (['category', 'regions'])
            ->byAuthor (Auth::user ())
            ->orderBy('created_at', 'DESC')
            ->paginate(20);

        return view('cabinet.advert.list', compact('adverts'));
    }

    public function create()
    {
        $categoriesTree = CategoryAdvert::getCategoryTree ();

        $regionsTree = Region::getTree();

        return view('cabinet.advert.create', ['categoriesTree'=>$categoriesTree,'regionsTree'=>$regionsTree]);
    }

    public function send(Advert $advert)
    {
        if(!Gate::allows('owner-advert', $advert)){
            abort(403);
        }

        try {
            $this->advertService->sendToModeration($advert->id);
        } catch (\DomainException $e) {
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('adverts.show', $advert);
    }

    public function close(Advert $advert)
    {
        if(!Gate::allows('owner-advert', $advert)){
            abort(403);
        }

        try {
            $this->advertService->close($advert->id);
        } catch (\DomainException $e) {
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('adverts.show', $advert);
    }

    public function store(CreateRequest $request)
    {
        try {
            $advert = $this->advertService->create(
                Auth::id (),
                $request->get('region_id', null),
                $request->get('category_advert_id'),
                $request
            );
        } catch (ModelNotFoundException $e) {
            return back()->with('error', $e->getMessage());
        }
        return redirect()
            ->route('cabinet.advert.show', compact('advert'))
            ->with('Success', 'Success create advert');
    }

    public function show(Advert $advert)
    {
        if(!Gate::allows('owner-advert', $advert)){
            abort(403);
        }

        $user = Auth::user();

        return view('advert.show', compact('advert', 'user'));
    }

    public function edit(Advert $advert)
    {
        if(!Gate::allows('owner-advert', $advert)){
            abort(403);
        }

        $advert->load(['regions', 'category', 'values']);

        $regionsTree = Region::getTree(null, $advert->regions->id);

        $categoriesTree = CategoryAdvert::getCategoryTree ($advert->category->id);

        return view('cabinet.advert.edit', [
            'categoriesTree' => $categoriesTree,
            'regionsTree' => $regionsTree,
            'advert' => $advert,
            'attributes' => $advert->category->allAttributesWithValues($advert->category->allAttributes(), $advert),
        ]);
    }

    public function update(CreateRequest $request, Advert $advert)
    {
        if(!Gate::allows('owner-advert', $advert)){
            abort(403);
        }

        try {
            //update after validation data
            $this->advertService->update($advert->id, $request);
        }catch (ModelNotFoundException $e) {
            return back()->with('error', $e->getMessage());
        }

        return redirect()->route('cabinet.advert.show', compact('advert'));
    }

    public function destroy(Advert $advert)
    {
        if(!Gate::allows('owner-advert', $advert)){
            abort(403);
        }

        $advert->delete();

        return redirect()->route('cabinet.advert.index');
    }

    public function attributesByCategory(Request $request)
    {
        $category = CategoryAdvert::findOrFail($request->get('category'));

        return response()->json($category->allAttributes());
    }

    public function photoUpload(PhotoCreateRequest $request, Advert $advert)
    {
        $imageName = time().'.'.$request->photo->getClientOriginalExtension();

        $request->photo->move(public_path('photo'), $imageName);

        return response()->json(['success'=>'You have successfully upload image.']);
    }
}