<?php

namespace App\Http\Controllers\Cabinet;

use App\Events\Advert\Message;
use App\Http\Requests\Cabinet\ChatMessage\SendMessageRequest;
use App\Service\ChatMessageService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ChatMessageController extends Controller
{
    protected $chatMessageService;

    public function __construct(ChatMessageService $chatMessageService)
    {
        //$this->middleware('auth');

        $this->chatMessageService = $chatMessageService;
    }

    public function advert(Request $request)
    {
        //personal messages on advert(between owner and client)
        //$this->chatMessageService->sendMessageOnAdvert (auth ()->user ()->id,);
        //event (new \App\Events\Message());

        broadcast (new \App\Events\Message($request))->toOthers ();


    }
}
