<?php


namespace App\Http\Controllers\Cabinet;

use App\Entity\Advert;
use App\Entity\PhotoAdvert;
use App\Http\Middleware\IsFilledProfile;
use App\Http\Requests\Cabinet\Photo\DeleteRequest;
use App\Service\AdvertService;
use App\Http\Requests\Cabinet\Photo\CreateRequest;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Storage;

class AdvertPhotoController extends Controller
{
    protected $advertService;

    public function __construct(AdvertService $advertService)
    {
        $this->middleware(IsFilledProfile::class);
        /** @var AdvertService advertService */
        $this->advertService = $advertService;
    }

    public function index(Advert $advert)
    {
        return view('cabinet.advert.photo.list', compact('advert'));
    }

    public function delete(DeleteRequest $request, Advert $advert)
    {
        $this->advertService->deletePhoto($request, $advert->id);

        return back()->with('success', 'Complete remove photo');
    }

    public function upload(CreateRequest $request, Advert $advert)
    {
        $this->advertService->addPhoto($advert->id, $request);

        return back()->with('success', 'Complete upload photo');
    }
}