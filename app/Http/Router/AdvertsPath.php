<?php

namespace App\Http\Router;

use Illuminate\Contracts\Routing\UrlRoutable;
use Illuminate\Support\Facades\Cache;
use App\Entity\Region;
use App\Entity\CategoryAdvert;

class AdvertsPath implements UrlRoutable
{
    /**
     * @var Region
     */
    public $region;

    /**
     * @var CategoryAdvert
     */
    public $category;

    public function withRegion(?Region $region): self
    {
        $clone = clone $this;
        $clone->region = $region;
        return $clone;
    }

    public function withCategory(?CategoryAdvert $category): self
    {
        $clone = clone $this;
        $clone->category = $category;
        return $clone;
    }

    public function getRouteKey()
    {
        $segments = [];
        if ($this->region) {
            $segments[] = Cache::tags(Region::class)->rememberForever('region_path_' . $this->region->id, function () {
                return $this->region->getPath();
            });
        }
        if ($this->category) {
            $segments[] = Cache::tags(CategoryAdvert::class)->rememberForever('category_path_' . $this->category->id, function () {
                return $this->category->getPath();
            });
        }
        return implode('/', $segments);
    }

    public function getRouteKeyName(): string
    {
        return 'adverts_path';
    }

    public function resolveRouteBinding($value)
    {
        $chunks = explode('/', $value);
        /** @var Region|null $region */
        $region = null;
        do {
            $slug = reset($chunks);
            if ($slug && $next = Region::where('slug', $slug)->where('parent_id', $region ? $region->id : null)->first()) {
                $region = $next;
                array_shift($chunks);
            }
        } while (!empty($slug) && !empty($next));
        /** @var CategoryAdvert|null $category */
        $category = null;
        do {
            $slug = reset($chunks);
            if ($slug && $next = CategoryAdvert::where('slug', $slug)->where('parent_id', $category ? $category->id : null)->first()) {
                $category = $next;
                array_shift($chunks);
            }
        } while (!empty($slug) && !empty($next));
        if (!empty($chunks)) {
            abort(404);
        }
        return $this
            ->withRegion($region)
            ->withCategory($category);
    }
}