<?php

namespace App\Http\Requests\Admin\Ticket;

use App\Entity\Ticket;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SearchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'int',
            'user' => 'int',
            'status' => ['string', Rule::in(Ticket::getStatusList())],
        ];
    }
}
