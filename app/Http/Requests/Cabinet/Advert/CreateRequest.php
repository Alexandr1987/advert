<?php

namespace App\Http\Requests\Cabinet\Advert;

use App\Entity\CategoryAdvert;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateRequest extends FormRequest
{
    protected $labelsForAttributes = [];

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $attributesRules = $this->getAttributeRules();

        $mainRules = [
            'title' => 'required|string',
            'content' => 'required|string|min:20',
            'price' => 'required|integer|min:10',
            'address' => 'required|string',
            'category_advert_id' => 'required|integer',
            'region_id' => 'required|integer',
        ];

        return array_merge($mainRules, $attributesRules);
    }

    protected function getAttributeRules(): array
    {
        $attributesRules = [];

        foreach ($this->all() as $param=>$value){
            //category selected and not empty
            if($param == "category_advert_id" && !empty($value)){

                $category = CategoryAdvert::findOrFail($value);

                if($category){
                    foreach ($category->allAttributes() as $attribute) {
                        $rules = [
                            $attribute->required ? 'required' : 'nullable',
                        ];
                        if ($attribute->isInteger()) {
                            $rules[] = 'integer';
                        } elseif ($attribute->isFloat()) {
                            $rules[] = 'numeric';
                        } else {
                            $rules[] = 'string';
                            $rules[] = 'max:255';
                        }
                        if ($attribute->isSelectList()) {
                            $rules[] = Rule::in($attribute->variants);
                        }
                        $attributesRules['attribute_' . $attribute->id] = $rules;

                        //label for error validation
                        $this->labelsForAttributes['attribute_' . $attribute->id] = $attribute->name;
                    }
                }
            }
        }

        return $attributesRules;
    }

    public function attributes()
    {
        $default =  ['region_id' => 'Region','category_advert_id' => 'Category'];

        return array_merge($default, $this->labelsForAttributes);
    }
}
