<?php

namespace App\Http\Middleware;

use App\Entity\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class IsFilledProfile
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /** @var User $user */
        $user = Auth::user();

        if(!$user->isFilledProfile()){
            return redirect()
                ->route('cabinet.profile.index')
                ->with('error', 'Please, fill all user profile fields');
        }

        return $next($request);
    }
}
