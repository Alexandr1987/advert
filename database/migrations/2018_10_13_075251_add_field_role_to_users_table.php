<?php

use App\Entity\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddFieldRoleToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            //add field role
            $table->string ('role', 20)->nullable (false);
        });

        //update all users, set ROLE for exist users in table
        DB::table ('users')->update (['role'=>User::ROLE_USER]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //remove field from table users
            $table->dropColumn('role');
        });
    }
}
