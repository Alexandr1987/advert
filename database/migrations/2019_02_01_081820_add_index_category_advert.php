<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexCategoryAdvert extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('advert_category', function (Blueprint $table) {
            $table->index('slug','idx_slug_advert_category');
            $table->index('parent_id','idx_parent_id_advert_category');
        });


        Schema::table('advert', function (Blueprint $table) {
            $table->index('slug','idx_slug_advert');
            $table->index('author_id','idx_author_id_advert');
        });


        Schema::table('advert_category_attributes', function (Blueprint $table) {
            $table->index('advert_category_id','idx_advert_category_id_attributes');
        });

        Schema::table('advert_chat_message', function (Blueprint $table) {
            $table->index('user_id','idx_user_id_advert_chat_message');
            $table->index('advert_id','idx_advert_id_advert_chat_message');
        });

        Schema::table('advert_photo', function (Blueprint $table) {
            $table->index('status','idx_advert_photo_status');
            $table->index('advert_id','idx_advert_id_advert_photo');
        });

        Schema::table('advert_values', function (Blueprint $table) {
            $table->index('advert_id','idx_advert_values_advert_id');
            $table->index('attribute_id','idx_attribute_id_advert_values');

        });

        Schema::table('adverts_favorite', function (Blueprint $table) {
            $table->index('user_id','idx_user_id_adverts_favorite');
            $table->index('advert_id','idx_advert_id_adverts_favorite');

        });

        Schema::table('tickets', function (Blueprint $table) {
            $table->index('user_id','idx_user_id_tickets');
            $table->index('status','idx_status_tickets');
        });


        Schema::table('tickets_messages', function (Blueprint $table) {
            $table->index('ticket_id','idx_ticket_id_tickets_messages');
            $table->index('user_id','idx_user_id_tickets_messages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
