<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvertTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);

        Schema::create('advert', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 255)->nullable (false);
            $table->string('slug')->nullable (false);
            $table->text ('content');
            $table->integer('category_advert_id')->references('id')->on('advert_category')->onDelete('CASCADE');
            $table->integer('region_id')->nullable()->references('id')->on('regions');
            $table->integer('author_id')->references('id')->on('users')->onDelete('CASCADE');
            $table->string('status', 16);
            $table->text('reject_reason')->nullable();
            $table->timestamps();
            $table->timestamp('published_at')->nullable();
            $table->timestamp('expires_at')->nullable();
            $table->integer('price');
            $table->text('address');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advert');
    }
}
