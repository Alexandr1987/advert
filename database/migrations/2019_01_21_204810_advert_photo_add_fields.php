<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AdvertPhotoAddFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('advert_photo', function (Blueprint $table) {
            $table->text('public_full_link')->nullable(false);
            $table->string('storage', 32)->nullable(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('advert_photo', function (Blueprint $table) {
            $table->dropColumn('public_full_link');
            $table->dropColumn('storage');
        });
    }
}
