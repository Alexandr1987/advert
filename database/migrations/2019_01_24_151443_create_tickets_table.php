<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subject', 256)->nullable(false);
            $table->integer('user_id')->references('id')->on('users')->onDelete('CASCADE');
            $table->text('message');
            $table->string('status', 16);
            $table->timestamps();
        });

        Schema::create('tickets_messages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ticket_id')->references('id')->on('tickets')->onDelete('CASCADE');
            $table->integer('user_id')->references('id')->on('users')->onDelete('CASCADE');
            $table->text('message');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets_messages');
        Schema::dropIfExists('tickets');
    }
}
