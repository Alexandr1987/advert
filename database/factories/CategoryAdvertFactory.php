<?php

use Faker\Generator as Faker;

$factory->define(\App\Entity\CategoryAdvert::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->name,
        'slug' => $faker->unique()->slug(2),
        'parent_id' => null,
    ];
});
