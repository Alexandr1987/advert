<?php

use Faker\Generator as Faker;
use App\Entity\User;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {

    $active = $faker->boolean;

    return [
        'name' => $faker->name,
        'last_name' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'phone' => substr($faker->unique ()->phoneNumber, 0, 15),
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
        'status' => ($active) ? User::STATUS_CONFIRM: User::STATUS_WAIT,
        'verify_string' => ($active) ? null : str_random(32),
        //add random role from role-list for active user
        'role' => ($active) ? $faker->randomElement(User::getRoleList()) : User::ROLE_USER,
    ];
});
