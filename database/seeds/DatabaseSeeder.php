<?php

use Illuminate\Database\Seeder;
use UserTableSeeder as UserTableSeederAlias;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //create users
        $this->call(UserTableSeederAlias::class);

        //create regions
        $this->call(RegionTableSeeder::class);

        //create category
        $this->call(CategoryAdvertTableSeeder::class);
    }
}
