<?php

use Illuminate\Database\Seeder;
use App\Entity\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        //create test data - users list
        factory(User::class, 10)->create();
    }
}
