<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Entity{
/**
 * App\Entity\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $status
 * @property string $role
 * @property string $last_name
 * @property string $phone
 * @property string $password
 * @property string $remember_token
 * @property string $verify_string;
 * @property string|null $email_verified_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereRole($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\User whereVerifyString($value)
 */
	class User extends \Eloquent {}
}

namespace App\Entity{
/**
 * App\Entity\Region
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property integer $parent_id
 * @property Region $parent
 * @property Region $children
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Region findSimilarSlugs($attribute, $config, $slug)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Region whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Region whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Region whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Region whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Region whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Region whereUpdatedAt($value)
 */
	class Region extends \Eloquent {}
}

namespace App\Entity{
/**
 * App\Entity\Advert
 *
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property string $status
 * @property string $content
 * @property integer $category_advert_id
 * @property integer $region_id
 * @property integer $author_id
 * @property string $reject_reason
 * @property integer $price;
 * @property string $address
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $published_at
 * @property integer $expires_at
 * @property-read \App\Entity\CategoryAdvert $category
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entity\PhotoAdvert[] $photos
 * @property-read \App\Entity\Region|null $region
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Advert byActive()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Advert byAuthor()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Advert byCategory(\App\Entity\CategoryAdvert $category)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Advert byRegion(\App\Entity\Region $region)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Advert whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Advert whereAuthorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Advert whereCategoryAdvertId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Advert whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Advert whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Advert whereExpiresAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Advert whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Advert wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Advert wherePublishedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Advert whereRegionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Advert whereRejectReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Advert whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Advert whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Advert whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\Advert whereUpdatedAt($value)
 */
	class Advert extends \Eloquent {}
}

namespace App\Entity{
/**
 * App\Entity\PhotoAdvert
 *
 * @property int $id
 * @property int $advert_id
 * @property string $file
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\PhotoAdvert whereAdvertId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\PhotoAdvert whereFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\PhotoAdvert whereId($value)
 */
	class PhotoAdvert extends \Eloquent {}
}

namespace App\Entity{
/**
 * App\Entity\CategoryAdvert
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property integer $parent_id
 * @property int $depth
 * @property CategoryAdvert $parent
 * @property CategoryAdvert[] $children
 * @property CategoryAdvertAttributes[] $attributes
 * @property int $_lft
 * @property int $_rgt
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\CategoryAdvert d()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\CategoryAdvert whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\CategoryAdvert whereLft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\CategoryAdvert whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\CategoryAdvert whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\CategoryAdvert whereRgt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\CategoryAdvert whereSlug($value)
 */
	class CategoryAdvert extends \Eloquent {}
}

namespace App\Entity{
/**
 * App\Entity\CategoryAdvertAttributes
 *
 * @property int $id
 * @property string $name
 * @property integer $advert_category_id
 * @property string $type
 * @property bool $required
 * @property array $variants
 * @property integer $sort
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\CategoryAdvertAttributes whereAdvertCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\CategoryAdvertAttributes whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\CategoryAdvertAttributes whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\CategoryAdvertAttributes whereRequired($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\CategoryAdvertAttributes whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\CategoryAdvertAttributes whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entity\CategoryAdvertAttributes whereVariants($value)
 */
	class CategoryAdvertAttributes extends \Eloquent {}
}

