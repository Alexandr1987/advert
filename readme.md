
## Config project:
<p align="center">
Create ".env" file in root dir:
APP_NAME=Laravel
APP_ENV=local
APP_KEY=base64:6V1bmvkjGWOg2+CUBXbZB+iU6uzOOwwv7dIVYZ21itk=
APP_DEBUG=true
APP_LOG_LEVEL=debug
APP_URL=http://localhost

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=advert
DB_USERNAME=root
DB_PASSWORD=root

BROADCAST_DRIVER=log
CACHE_DRIVER=file
SESSION_DRIVER=file
QUEUE_DRIVER=sync

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_DRIVER=log
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
</p>

<p>
Run commands:
 php artisan config:cache
 php artisan migrate:refresh
</p>

<p>
sudo apt-get install -y nodejs

node -v

npm -v

npm install -g yarn

sudo npm install --save-dev cross-env

sudo npm install --no-bin-links

sudo yarn install

sudo npm dev
-----IF have problem with laravel-mix------
rm node_module 
npm install
npm rebuild node-sass 
</p

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
